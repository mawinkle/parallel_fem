
#include "TB/Gdx.hpp"
#include "TB/Util.hpp"

using namespace TB;

void ShaderProgram::print(std::string _id, ShaderProgram::Status _compComp, ShaderProgram::Status _compVert,
	ShaderProgram::Status _compGeom, ShaderProgram::Status _compFrag, ShaderProgram::Status _link, std::string _errorLog) {
	if (!printDebug) return;
	LOG("   Shader: " + std::string(_id));
	LOG("Compiling: "
		+ std::string(_compComp == Status::failed ? " X |" : _compComp == Status::success ? " S |" : " - |")
		+ std::string(_compVert == Status::failed ? " X |" : _compVert == Status::success ? " S |" : " - |")
		+ std::string(_compGeom == Status::failed ? " X |" : _compGeom == Status::success ? " S |" : " - |")
		+ std::string(_compFrag == Status::failed ? " X |" : _compFrag == Status::success ? " S |" : " - |")
		+ "\n");
	LOG("  Linking: " + std::string(_link == Status::failed ? "Failed!" : _link == Status::success ? "Success!" : " - ") + "\n");

	if (_errorLog.empty()) {
		LOG("\n");
	} else {
		LOG("\n" + std::string(_errorLog) + "\n");
	}

	GLError(_id);
	std::cout << std::endl;
}

bool ShaderProgram::compile(const std::string& _id, const char* _compute, const char* _vertex, const char* _geom, const char* _frag) {
	Status compStatus = Status::missing;
	Status vertStatus = Status::missing;
	Status geomStatus = Status::missing;
	Status fragStatus = Status::missing;
	Status linkStatus = Status::missing;

	if (compute != -1) {
		glDeleteShader(compute);
		compute = -1;
	}
	if (vertex != -1) {
		glDeleteShader(vertex);
		vertex = -1;
	}
	if (geom != -1) {
		glDeleteShader(geom);
		geom = -1;
	}
	if (frag != -1) {
		glDeleteShader(frag);
		frag = -1;
	}
	if (program != -1) {
		glDeleteShader(program);
		program = -1;
	}

	//Compile Compute
	if (_compute != nullptr) {
		compute = glCreateShader(GL_COMPUTE_SHADER);
		glShaderSource(compute, 1, &_compute, nullptr);
		glCompileShader(compute);
		GLint isCompiled = 0;
		glGetShaderiv(compute, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE) {
			GLint maxLength = 0;
			glGetShaderiv(compute, GL_INFO_LOG_LENGTH, &maxLength);
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(compute, maxLength, &maxLength, &errorLog[0]);
			glDeleteShader(compute);
			compStatus = Status::failed;
			print(_id, compStatus, vertStatus, geomStatus, fragStatus, linkStatus, std::string(errorLog.begin(), errorLog.end()));
			return false;
		} else compStatus = Status::success;
	}

	//Compile Vertex
	if (_vertex != nullptr) {
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &_vertex, nullptr);
		glCompileShader(vertex);
		GLint isCompiled = 0;
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE) {
			GLint maxLength = 0;
			glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &maxLength);
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(vertex, maxLength, &maxLength, &errorLog[0]);
			glDeleteShader(vertex);
			vertStatus = Status::failed;
			print(_id, compStatus, vertStatus, geomStatus, fragStatus, linkStatus, std::string(errorLog.begin(), errorLog.end()));
			return false;
		} else vertStatus = Status::success;
	}

	//Compile Geom
	if (_geom != nullptr) {
		geom = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geom, 1, &_geom, nullptr);
		glCompileShader(geom);
		GLint isCompiled = 0;
		glGetShaderiv(geom, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE) {
			GLint maxLength = 0;
			glGetShaderiv(geom, GL_INFO_LOG_LENGTH, &maxLength);
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(geom, maxLength, &maxLength, &errorLog[0]);
			glDeleteShader(geom);
			geomStatus = Status::failed;
			print(_id, compStatus, vertStatus, geomStatus, fragStatus, linkStatus, std::string(errorLog.begin(), errorLog.end()));
			return false;
		} else geomStatus = Status::success;
	}

	//Compile Frag
	if (_frag != nullptr) {
		frag = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(frag, 1, &_frag, nullptr);
		glCompileShader(frag);
		GLint isCompiled = 0;
		glGetShaderiv(frag, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE) {
			GLint maxLength = 0;
			glGetShaderiv(frag, GL_INFO_LOG_LENGTH, &maxLength);
			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(frag, maxLength, &maxLength, &errorLog[0]);
			glDeleteShader(frag);
			fragStatus = Status::failed;
			print(_id, compStatus, vertStatus, geomStatus, fragStatus, linkStatus, std::string(errorLog.begin(), errorLog.end()));
			return false;
		} else fragStatus = Status::success;
	}

	//Link
	program = glCreateProgram();
	if (_compute != nullptr) glAttachShader(program, compute);
	if (_vertex != nullptr) glAttachShader(program, vertex);
	if (_geom != nullptr) glAttachShader(program, geom);
	if (_frag != nullptr) glAttachShader(program, frag);

	glLinkProgram(program);

	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE) {
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> errorLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &errorLog[0]);
		if (compute != -1)glDeleteShader(compute);
		if (vertex != -1)glDeleteShader(vertex);
		if (geom != -1)glDeleteShader(geom);
		if (frag != -1)glDeleteShader(frag);
		if (program != -1) glDeleteProgram(program);
		linkStatus = Status::failed;
		print(_id, compStatus, vertStatus, geomStatus, fragStatus, linkStatus, std::string(errorLog.begin(), errorLog.end()));
		return false;
	} else linkStatus = Status::success;

	if (_compute != nullptr)glDetachShader(program, compute);
	if (_vertex != nullptr)glDetachShader(program, vertex);
	if (_geom != nullptr)glDetachShader(program, geom);
	if (_frag != nullptr)glDetachShader(program, frag);

	print(_id, compStatus, vertStatus, geomStatus, fragStatus, linkStatus, "");
	unbind();
	return true;
}

GLuint ShaderProgram::getHandle() {
	return program;
}

bool ShaderProgram::loadFromMemory(const std::string& _id, const std::string& _compute, const std::string& _vertex, const std::string& _geom, const std::string& _frag) {
	return compile(_id, _compute.empty() ? nullptr : _compute.c_str(), _vertex.empty() ? nullptr : _vertex.c_str(), _geom.empty() ? nullptr : _geom.c_str(), _frag.empty() ? nullptr : _frag.c_str());
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(program);
    program = 0;
}

ShaderProgram::ShaderProgram() {}

ShaderProgram::ShaderProgram(std::string _path) {
    bool cExists = true;
	bool vExists = true;
	bool gExists = true;
	bool fExists = true;

	std::ifstream comp(_path + ".comp");
	cExists = comp.good();

	std::ifstream vert(_path + ".vert");
	vExists = vert.good();

	std::ifstream geom(_path + ".geom");
	gExists = geom.good();

	std::ifstream frag(_path + ".frag");
	fExists = frag.good();

    loadFromMemory(_path, 
        (cExists ? std::string{ std::istreambuf_iterator<char>(comp), std::istreambuf_iterator<char>() } : ""), 
        (vExists ? std::string{ std::istreambuf_iterator<char>(vert), std::istreambuf_iterator<char>() } : ""), 
        (gExists ? std::string{ std::istreambuf_iterator<char>(geom), std::istreambuf_iterator<char>() } : ""), 
        (fExists ? std::string{ std::istreambuf_iterator<char>(frag), std::istreambuf_iterator<char>() } : ""));

	comp.close();
	vert.close();
	geom.close();
	frag.close();
}

void ShaderProgram::bind() {
	glUseProgram(getHandle());
	GLError("ShaderProgram::bind");
}

void ShaderProgram::unbind() {
	glUseProgram(0);
	GLError("ShaderProgram::unbind");
}

SSBO::SSBO(std::string _id, uint _size, void* _data, GLbitfield _flags) : flags(_flags) {
    glGenBuffers(1, &handle);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, handle);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, _size, _data, _flags);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	GLError("SSBO::glLoad::" + id);
}

SSBO::~SSBO() {
    unmap();
	glDeleteBuffers(1, &handle);
	GLError("SSBO::glUnload::" + id);
}

void SSBO::bind(uint _binding) {
	bindAs(GL_SHADER_STORAGE_BUFFER, _binding);
}

void SSBO::bindAs(uint _target, uint _binding) {
	glBindBufferBase(lastBindTarget = _target, _binding, handle);
	GLError("SSBO::bindAs::" + id);
}

void SSBO::unbind() {
	glBindBuffer(lastBindTarget, 0);
	GLError("SSBO::unbind::" + id);
}

void* SSBO::map(uint _offset, uint _length, GLbitfield _flags) {
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, handle);
	pntr = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, _offset, _length, _flags);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	GLError("SSBO::map::" + id);
	return pntr;
}

void SSBO::unmap() {
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, handle);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	GLError("SSBO::unmap::" + id);
}

FlipFlopSSBO::FlipFlopSSBO(std::string _id, bool _autoflip, uint _bufferCount, uint _size, GLbitfield _bufferFlags, GLbitfield _mapFlags) {
	buffers.resize(_bufferCount);
	pointers.resize(_bufferCount);
	for (uint i = 0; i < _bufferCount; ++i){
		buffers[i] = new SSBO(_id + "_" + std::to_string(i), _size, nullptr, _bufferFlags);
        pointers[i] = buffers[i]->map(0, _size, flags);
    }
    //TODO autoflip needed??
}

FlipFlopSSBO::~FlipFlopSSBO(){
    for(SSBO* buffer : buffers)
        delete buffer;
}

void FlipFlopSSBO::flip() {
	index = (index++) % size;
}

void FlipFlopSSBO::bind(uint _binding) {
	bindAs(GL_SHADER_STORAGE_BUFFER, _binding);
}

void FlipFlopSSBO::bindAs(uint _target, uint _binding) {	
	buffers[index]->bindAs(_target, _binding);
}

void FlipFlopSSBO::unbind() {
	buffers[index]->unbind();
}

Framebuffer::Framebuffer(std::string _id, std::unordered_map<std::string, Texture2D*> _textures) {
    
	glGenFramebuffers(1, &handle);
	glBindFramebuffer(GL_FRAMEBUFFER, handle);
	uint k = 0;
	//https://www.khronos.org/opengl/wiki/GLAPI/glFramebufferTexture
	for (auto& it : textures){
		Texture2D* tex = it.second;
		if (k == 0)
			bounds = Vec2u(tex->bounds);
		if (tex->format == GL_DEPTH_COMPONENT || tex->format == GL_DEPTH_STENCIL || tex->format == GL_STENCIL_INDEX)
			glFramebufferTexture2D(GL_FRAMEBUFFER, tex->format, GL_TEXTURE_2D, tex->get(), tex->level);
		else
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + k++, GL_TEXTURE_2D, tex->get(), tex->level);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GLError("ShadowMap::glLoad");
}

Framebuffer::~Framebuffer () {
    for (auto& it : textures)
		delete it.second;
	glDeleteFramebuffers(1, &handle);
	GLError("Framebuffer::glUnload");
}

void Framebuffer::bind() {	
	glBindFramebuffer(GL_FRAMEBUFFER, handle);
	glViewport(0, 0, bounds.x, bounds.y);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLError("Framebuffer::bind");
}

void Framebuffer::unbind() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, M_WIDTH, M_HEIGHT);
	GLError("ShadowMap::unbind");
}