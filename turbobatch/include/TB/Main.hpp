/**
 * Root node of the topologically sorted include graph. 
 * Only includes and forward declerations in this file!
 */

#pragma once

#define GLEW_STATIC
#define GLM_ENABLE_EXPERIMENTAL

#include <GL/glew.h>

//muemer no sälber builde da em standart package eifach 90% vo de einzeln häder fehler
//absolute gugus
#include <glm/glm.hpp>

//#include <glm/ext/vector_float2.hpp>
//#include <glm/ext/vector_float3.hpp>
//#include <glm/ext/vector_float4.hpp>
//#include <glm/ext/vector_uint2.hpp>
//#include <glm/ext/vector_uint3.hpp>
//#include <glm/ext/vector_uint4.hpp>
//#include <glm/ext/vector_int2.hpp>
//#include <glm/ext/vector_int3.hpp>
//#include <glm/ext/vector_int4.hpp>
//#include <glm/ext/matrix_float4x4.hpp>

#include <glm/gtx/quaternion.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <nanogui/opengl.h>
#include <nanogui/glutil.h>
#include <nanogui/screen.h>
#include <nanogui/window.h>
#include <nanogui/layout.h>
#include <nanogui/label.h>
#include <nanogui/checkbox.h>
#include <nanogui/button.h>
#include <nanogui/toolbutton.h>
#include <nanogui/popupbutton.h>
#include <nanogui/combobox.h>
#include <nanogui/progressbar.h>
#include <nanogui/entypo.h>
#include <nanogui/messagedialog.h>
#include <nanogui/textbox.h>
#include <nanogui/slider.h>
#include <nanogui/imagepanel.h>
#include <nanogui/imageview.h>
#include <nanogui/vscrollpanel.h>
#include <nanogui/colorwheel.h>
#include <nanogui/graph.h>
#include <nanogui/tabwidget.h>
#include <nanogui/glcanvas.h>

#include <iostream>
#include <string>
#include <fstream>

namespace TB {

typedef unsigned int uint;
typedef unsigned short ushort;

typedef glm::ivec2 Vec2i;
typedef glm::ivec3 Vec3i;
typedef glm::ivec4 Vec4i;

typedef glm::uvec2 Vec2u;
typedef glm::uvec3 Vec3u;
typedef glm::uvec4 Vec4u;

typedef glm::vec2 Vec2;
typedef glm::vec3 Vec3;
typedef glm::vec4 Vec4;

typedef glm::mat3 Mat3;
typedef glm::mat4 Mat4;

typedef glm::quat Quat;

#define M_WIDTH 0
#define M_HEIGHT 0

    //Gdx
    class ShaderProgram;
    class SSBO;
    class FlipFlopSSBO;
    class Texture2D;
    class Framebuffer;

    //Base

    //Util

    
}