/**
 * Home of various openGL helper classes
 * !!CONSTRUCTOR CALLS ONLY SUPPORTED ON RENDER THREAD!!
 */

#pragma once

#include "TB/Main.hpp"

namespace TB {

    class ShaderProgram {
		enum Status {
			success, failed, missing
		};
		GLuint program = -1, compute = -1, vertex = -1, geom = -1, frag = -1;
		void print(std::string, Status, Status, Status, Status, Status, std::string);
		bool compile(const std::string&, const char*, const char*, const char*, const char*);
		
	public:
		ShaderProgram(std::string);
        ShaderProgram();
        ~ShaderProgram();

        bool loadFromMemory(const std::string&, const std::string&, const std::string&, const std::string&, const std::string&);
		bool printDebug = true;
		GLuint getHandle();		
		void bind();
		void unbind();
	};

    class SSBO {
        const std::string id;
		GLuint handle;
		GLbitfield flags;
		uint lastBindTarget;
		void* pntr;

	public:
		/*
		https://www.khronos.org/opengl/wiki/GLAPI/glBufferStorage
		GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, GL_MAP_WRITE_BIT, GL_MAP_PERSISTENT_BIT,
		GL_MAP_COHERENT_BIT, GL_CLIENT_STORAGE_BIT
		*/
		SSBO(std::string, uint, void*, GLbitfield);
        ~SSBO();
		void bind(uint);
		void bindAs(uint, uint);
		void unbind();
		/*
		https://www.khronos.org/opengl/wiki/GLAPI/glMapBufferRange
		!offset and length in byte!
		GL_MAP_READ_BIT, GL_MAP_WRITE_BIT, GL_MAP_INVALIDATE_RANGE_BIT,
		GL_MAP_INVALIDATE_BUFFER_BIT, GL_MAP_FLUSH_EXPLICIT_BIT,
		GL_MAP_UNSYNCHRONIZED_BIT, GL_MAP_PERSISTENT_BIT,
		GL_MAP_COHERENT_BIT
		*/
		void* map(uint, uint, GLbitfield);
		void unmap();		

		template<class T>
		T inline getPtr() {
			return reinterpret_cast<T>(pntr);
		};

	};

    class FlipFlopSSBO{
		GLbitfield flags;
		uint index = 0, size;
		std::vector<SSBO*> buffers;
		std::vector<void*> pointers;

	public:
		FlipFlopSSBO(std::string, bool, uint, uint, GLbitfield, GLbitfield);
        ~FlipFlopSSBO();
		void flip();
		void bind(uint);
		void bindAs(uint, uint);
		void unbind();
		template<class T>
		T inline getPtr() {
			return reinterpret_cast<T>(pointers[index]);
		};
	};

    class Texture2D {
		friend Framebuffer;

		GLuint handle;
		GLenum target, format, type;
		GLint level, internalFormat;
		Vec2u bounds;

	public:
		//https://www.khronos.org/opengl/wiki/GLAPI/glTexImage2D
		//target, level, internalFormat, format, type
		Texture2D(std::string, GLuint, GLint, GLint, GLenum, GLenum);
        ~Texture2D();
		GLuint get();
		void bind(GLuint);
		void setWrap(GLint, GLint);
		void setFilter(GLint, GLint);
		void setParai(GLenum, GLint);
		void setParaf(GLenum, GLfloat);
		static Texture2D* get(std::string);
	};

    class Framebuffer{
		GLuint handle;
		std::unordered_map<std::string, Texture2D*> textures;
	public:
		Vec2u bounds;
		Framebuffer(std::string, std::unordered_map<std::string, Texture2D*>);
        ~Framebuffer();
		void bind();
		void unbind();
		Texture2D* getTex(std::string);
		static Framebuffer* get(std::string);
	};

}
