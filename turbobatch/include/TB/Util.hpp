#pragma once

#include "TB/Main.hpp"

namespace TB {

#define LOG(X) std::cout << X << std::endl;
#define GLError(X) (printOpenGlErrors((X)))
#define FBError(X) (printFrameBufferErrors((X)))

    inline void printOpenGlErrors(std::string _id) {
        bool hasError = false;
        GLenum err;
        while ((err = glGetError()) != GL_NO_ERROR) {
            if (!hasError) {
                hasError = true;
                std::cout << "---- Print OpenGl Errors: " << _id << " ----" << std::endl;
            }
            if (err == GL_INVALID_VALUE)
                std::cout << "Invalid Value"; 
            else if(err == GL_INVALID_OPERATION)
                std::cout << "Invalid Operation";
            else if (err == GL_OUT_OF_MEMORY)
                std::cout << "Out of Memory";
            else if (err == GL_INVALID_ENUM)
                std::cout << "Invalid Enum";
            else if (err == GL_STACK_OVERFLOW)
                std::cout << "Stack Overflow";
            else if (err == GL_STACK_UNDERFLOW)
                std::cout << "Stack Underflow";
            else if (err == GL_INVALID_FRAMEBUFFER_OPERATION)
                std::cout << "Invalid Framebuffer Operation";
            else if (err == GL_CONTEXT_LOST)
                std::cout << "Context Lost";

            std::cout << " (" << err << ")" << std::endl;
        }
        if (hasError) std::cout << "---- Finished ----" << std::endl;
    };

    inline void printFrameBufferErrors(std::string _id) {
        switch (glCheckFramebufferStatus(GL_FRAMEBUFFER)) {
            case GL_FRAMEBUFFER_COMPLETE:
                return;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER INCOMPLETE ATTACHMENT" << std::endl;
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER INCOMPLETE MISSING ATTACHMENT" << std::endl;
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER INCOMPLETE DRAW BUFFER" << std::endl;
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER INCOMPLETE READ BUFFER" << std::endl;
                break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER UNSUPPORTED" << std::endl;
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER INCOMPLETE MULTISAMPLE" << std::endl;
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                std::cout << "---- Print Framebuffer Errors: " << _id << " ----" << std::endl;
                std::cout << "FRAMEBUFFER INCOMPLETE LAYER TARGETS" << std::endl;
                break;
            }
            std::cout << "---- Finished ----" << std::endl;
    };

}