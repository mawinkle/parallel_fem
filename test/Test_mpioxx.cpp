#include <mpi.h>
#include <cstdio>
#include <iostream>
#include <turbofem/src/mpioxx/mpiostream.hpp>

int main(int argc, char **argv)
{
	MPI_Init(NULL, NULL);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	char text[] = "Hello";
	if (world_rank != 0)
	{
		/*
		mpioxx::mpi_streambuf buf;
		std::ostream ost(&buf);
		ost << "Hello " << std::flush;
		*/
		std::cout << "MPI COMM RANK NOT 0 " << std::endl;
	}
	else
	{
		/*
		mpioxx::mpi_streambuf buf;
		std::istream ist(&buf);
		std::string str;
		ist >> str;
		std::cout << str << "\n";
		ist >> str;
		std::cout << str << "\n";
		*/
		std::cout << "MPI COMM RANK 0" << std::endl;
	}
	MPI_Finalize();
}