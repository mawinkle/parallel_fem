#include <turbofem/sparse_lu>
#include <iostream>
#include <cassert>

void doStuff(int matrixSize)
{
    Eigen::MatrixXd eigenMat = Eigen::MatrixXd::Random(matrixSize, matrixSize);
    
    // compute eigen matrizes
    Eigen::FullPivLU<Eigen::MatrixXd> pivLU(eigenMat);
    Eigen::MatrixXd eigenU = pivLU.matrixLU().triangularView<Eigen::Upper>();
    Eigen::MatrixXd eigenL(matrixSize, matrixSize);
    eigenL.setIdentity();
    eigenL.triangularView<Eigen::StrictlyLower>() = pivLU.matrixLU();
    Eigen::MatrixXd eigenP = pivLU.permutationP();
    // compute our own matrizes
    hash_matrix<double, row_major> sparseMat(eigenMat);

    try {
        sparse_lu<double> turbofemSolver = sparse_lu<double>(sparseMat);
        Eigen::MatrixXd turboFEM_L = turbofemSolver.L.toDense();
        Eigen::MatrixXd turboFEM_U = turbofemSolver.U.toDense();
        Eigen::MatrixXd recompTurboFEM = turboFEM_L * turboFEM_U;

        std::cout << matrixSize << "\t" << (turbofemSolver.P.toDense() * eigenMat - recompTurboFEM).norm() << std::endl;
    } catch (MatrixSingularException e) {
        std::cout << matrixSize << "\t" << e.what() << std::endl;
    } 
}

int main(int argc, char const *argv[])
{
    double tolerance = 1e-13;

    for (int i = 50; i < 60; i++)
    {
        doStuff(i);
    }

    return 0;
}
