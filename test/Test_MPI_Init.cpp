#include <mpi.h>
#include <iostream>

int main(int argc, char const *argv[])
{
    MPI_Init(nullptr, nullptr);
    int rank, size;

    // get rank and size
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    std::cout << "Size: " << size << std::endl << "Rank: " << rank << std::endl;

    MPI_Finalize();
    return 0;
}
