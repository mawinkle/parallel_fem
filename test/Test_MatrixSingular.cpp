#include <turbofem/sparse_lu>
#include <cassert>

int main(int argc, char const *argv[])
{
    Eigen::MatrixXd singMat = Eigen::MatrixXd::Zero(10,10);

    hash_matrix<double, row_major> hashMat(singMat);

    try {
        sparse_lu<double> solver(hashMat);
        assert(false);
    } catch (MatrixSingularException e) {
        assert(true);
    }

    return 0;
}