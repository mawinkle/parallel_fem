% IEEE standard conference template; to be used with:
%   spconf.sty  - LaTeX style file, and
%   IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass[letterpaper]{article}
\usepackage{spconf,amsmath,amssymb,graphicx}
\usepackage[ruled,vlined,procnumbered]{algorithm2e}

% bold paragraph titles
\newcommand{\mypar}[1]{{\bf #1.}}

% useful math constants
\newcommand{\R}{\mathbb{R}}

\title{TurboFEM - Parallel finite element methods}

\name{Matthias Busenhart, Richard Harpur, Damian Heer, Manuel Winkler}
\address{Department of Informatics\\ ETH Z\"urich\\Z\"urich, Switzerland}
\begin{document}
%\ninept
%
\maketitle

\begin{abstract}
The finite element method (FEM) is a widely used concept in numerical mathematics. FEM is a method to efficiently model physical processes. By using spatial discretization schemes based on a mesh, the solution of an analytically unsolvable partial differential equation (PDE) is approximated by the solution of a finite linear system of equations.

The most computational resources go into assembling the matrix describing this linear system of equations and factorizing it. The computations on the cells are ideal for parallelisation, as unconnected mesh cells are independent of each other.

In this paper, we present an implementation of a domain decomposition method. It can be used to solve partial differential equations iteratively. We conclude that our approach has superlinear speedup for a fixed mesh size, which is achieved by assembling and factorizing smaller matrices. We implement two versions, one based on message passing using MPI, the other one based on shared memory using pthreads. Both use SuperLU, a state-of-the-art LU-solver. We finally present our runtime model and show that our approach scales with superlinear speedup up to 18 cores and linearly up to 64 cores.
\end{abstract}

\section{Introduction}\label{sec:intro}
\mypar{Motivation} Solving a PDE can be exceedingly hard, especially for meshes with large numbers of nodes and cells. This problem is of practical relevance as such problems appear in various kinds of applications ranging from quantum physics to computational biology and computer graphics. As industry is interested in solving problems on larger and larger meshes, this demand motivates the utilization of parallel approaches.

\mypar{Related work} In our preliminary research in preparation for this paper we encountered various approaches. The main thrust in most of those approaches is to decompose the domain into several distinct subdomains \cite{SurveyOfParallelSolvers}. Additionally, other approaches utilize the possibility of parallelising the assembly of the system matrix \cite{DBLP:journals/corr/abs-1805-03949} or the sparse LU-factorization \cite{vollaire:98}.

\section{Background: FEM}\label{sec:background}
The main goal of FEM is to approximate the solution to a PDE. FEM achieves this by transforming the PDE into a linear system of equations (LSE). In this paper we consider the Poisson equation on the unit square $[0, 1] \times [0, 1] =: \Omega$ with zero boundary conditions, here given in variational formulation (also known as "weak" formulation):
$u \in H_0^1(\Omega)$:
\begin{equation}
    \label{eq:weak_divgradu}
    \int_\Omega\sigma (\nabla u) \cdot (\nabla v)dx = \int_\Omega fvdx
    \quad \forall v \in H_0^1(\Omega)
\end{equation}
where $H_0^1(\Omega)$ is a notation for a first-order Hilbert function space with zero boundary condition, $\sigma: \Omega \rightarrow \R$ is a stiffness coefficient function, and $f: \Omega \rightarrow \R$ is a load function.

This can be written as
\begin{equation}
    \label{eq:weak_functional}
    u \in H_0^1(\Omega): \quad
    a(u, v) = l(v) \quad
    \forall v \in H_0^1(\Omega)
\end{equation}
where $a$ is a symmetric positive definite bilinear form corresponding to the left-hand side of (\ref{eq:weak_divgradu}), and $l$ is a linear form corresponding to the right-hand side.

The solution $u$ is approximated by searching for the solution $u_h$ of the same problem, but on an $N$-dimensional subspace $H_{0, h}^1(\Omega) \subset H_0^1(\Omega)$, namely one in which all functions are piecewise polynomials on the triangular or quadrilateral cells ("elements" - hence finite element method) of a mesh.

A basis $\mathcal{B} = \{ b_1, b_2, \dots, b_N \}$ for $H_{0,h}^1(\Omega)$ is introduced. Now (\ref{eq:weak_functional}) is discretized to a LSE
\begin{equation}
    \label{eq:galerkin_system}
    A u_h = \phi
\end{equation}
where $A$ is the so-called Galerkin-matrix given by
\begin{equation}
    \label{eq:galerkin_entries}
    A_{ij} = a(b_j, b_i)
\end{equation}
and $\phi$ is the load vector given by
\begin{equation}
    \label{eq:load_entries}
    \phi_i = l(b_i)
\end{equation}
The basis functions are cleverly chosen such that the matrix $A$ becomes sparse, and such that each basis function $b_i$ is associated uniquely with one node in the mesh. We chose basis functions that are piecewise bilinear on the mesh cells, continuous and fulfil the cardinal basis property
\begin{equation}
    \label{eq:cardinal-basis}
    b_i(\xi_j) = 
    \begin{cases}
        1 & \text{if $i=j$}\\
        0 & \text{otherwise}
    \end{cases}
\end{equation}
$\xi_j$ being the position of node $j$.

\begin{algorithm}
\SetAlgoLined
\caption{Finite element method}
\label{algo:fem}
\BlankLine
assemble $A$ using (\ref{eq:galerkin_entries})\;
assemble $\phi$ using (\ref{eq:load_entries})\;
solve $Au=\phi$\;
\end{algorithm}

Thus a FEM implementation looks like Algorithm \ref{algo:fem}. Note that the integrals are approximated using numerical quadrature.

\mypar{Schwarz method}
The Schwarz method \cite{MR3450068} is a method to decompose a given domain $\Omega$ into subdomains $\Omega_i$, where 
\begin{equation}
    \bigcup_i \Omega_i = \Omega
\end{equation}
Next, the method applies FEM on each subdomain in parallel. These subdomains and the resulting Galerkin-matrix are smaller, and thus, assembling and solving takes less time. The only downside is that the resulting solutions need to be glued together to get the global solution.
This is done iteratively \cite{MR3450068}.

There are several approaches to gluing the solutions together. The one we chose is to use overlapping subdomains, such that the boundary of one subdomain $i$ is contained within its neighbours, i.e. for all neighbours $j$ of $i$:
\begin{equation}
    \Omega_i \cap \Omega_j \ne \varnothing
\end{equation}

For each step $n$ of the iteration and subdomain $i$, the solution of the neighbouring subdomains on the overlap is used as a boundary condition\footnote{There are several ways to enforce boundary conditions when solving $Ax = b$. 
We chose to set the rows in $A$ corresponding to nodes on the boundary to 0 outside of the diagonal, and to 1 on the diagonal. Then simply replace the corresponding entries of $b$ to the values we want to enforce.}. The resulting solution is used for the subsequent step:
\begin{equation}
A_i u_i^{n+1} = \phi_i \quad \text{ s.t } \quad u_i^{n+1} = u_j^n \text{ on }\Omega_i \cap \Omega_j
\end{equation}
This is repeated until the difference between $u_i^n$ and $u_j^n$ on $\Omega_i \cap \Omega_j$ falls below a certain tolerance.

The rate of convergence depends on the proportion of the overlap in relation to the subdomain, i.e. for a fixed subdomain size, a bigger overlap will lead to faster convergence. The basic Schwarz method is summarized in Algorithm \ref{algo:simple-schwarz}.

\begin{algorithm}
\SetAlgoLined
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Output{Solution $u$ for process $i$}
\Input{process $i$}
\SetKwProg{Fn}{Function}{:}{\KwRet{u$_i$}}
\SetKwFunction{FsolveOnSubDomain}{solveOnSubDomain}
\caption{Simple Schwarz method}
\label{algo:simple-schwarz}

\BlankLine
\Fn{\FsolveOnSubDomain{$i$}}{
    
    assemble $A_i$ on subdomain $i$\;
    assemble $\phi_i$ on subdomain $i$\;
    
    \Repeat{convergence}
    {
        exchange boundary values with neighbours\;
        apply boundary values\;
        solve $A_i u_i = \phi_i$\;
    }
}
\end{algorithm}

\begin{algorithm}
\SetAlgoLined
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Output{Solution $u$ for this rank}
\Input{rank $i$, mesh of subdomain $M_i$, error tolerance $\tau$}
\SetKwProg{Fn}{Function}{:}{\KwRet{u}}
\SetKwFunction{FsolveOnSubDomain}{solveOnSubDomain}
\SetKwFunction{distribute}{distribute}
\SetKwFunction{assembleMatrix}{assembleMatrix}
\SetKwFunction{assembleLoadVector}{assembleLoadVector}
\SetKwFunction{setBoundaryRowsToIdentity}{setBoundaryRowsToIdentity}
\SetKwFunction{MPIIsend}{MPI\_Isend}
\SetKwFunction{MPIIrecv}{MPI\_Irecv}
\SetKwFunction{MPIWait}{MPI\_Wait}
\SetKwFunction{MPIAllreduce}{MPI\_Allreduce}
\SetKwFunction{factorize}{factorize}
\SetKwFunction{solve}{solve}
\caption{FEM with Schwarz columnwise}
\label{algo:schwarz}

\BlankLine
\Fn{\FsolveOnSubDomain{$i$, $M_i$, $\tau$}}{
    
    $m \leftarrow M_i$.width\;
    $A_i$ $\leftarrow$ \assembleMatrix($M_i$)\;
    \setBoundaryRowsToIdentity($M_i$, $A_i$)\;
    $\phi_i$ $\leftarrow$ \assembleLoadVector($M_i$)\;
    
    SparseSolver.\factorize($A_i$)\;
    
    $u$.columns[2] = [0,\dots,0]\;
    $u$.columns[$m-1$] = [1,\dots,1]\;
    
    \While{$\varepsilon > \tau$}
    {
        $u_{-} \leftarrow$ $u$.columns[2]\;
        $u_{+} \leftarrow$ $u$.columns[$m-1$]\;
        \MPIIsend($i-1$, $u_{-}$)\;
        \MPIIsend($i+1$, $u_{+}$)\;
    
        $\zeta_{-} \leftarrow$ \MPIIrecv($i-1$)\;
        $\zeta_{+} \leftarrow$ \MPIIrecv($i+1$)\;
        \MPIWait()\;
        $e \leftarrow ||u_{-} - \zeta_{-}|| + ||u_{+} - \zeta_{+}||$\;
        $\varepsilon \leftarrow$ \MPIAllreduce($e$)\;
        
        $\phi_i$.columns[1] $\leftarrow$ $\zeta_{-}$\;
        $\phi_i$.columns[$m$] $\leftarrow$ $\zeta_{+}$\;
        
        $u \leftarrow$ SparseSolver.\solve($\phi_i$)\;
    }
}
\end{algorithm}

\section{Our implementation}\label{sec:our_implementation}
We implemented the Schwarz method on the unit square using a regular quadrilateral mesh (i.e. a mesh with only identical square cells), with $N \times N$ cells and thus $N+1 \times N+1$ mesh nodes. Subsequently, we divided the mesh along the x-axis into columns of equal width in such a way that the subdomains overlap by exactly one cell. We store the vertices in column-major-order, which leads to a column-major storage order for the solution vector $u$ and the load vector $\phi$. Thus, we just need to send one column in the boundary value exchange, which lies contiguously in memory, leveraging spatial locality. For quadrature, we used a nine-point two-dimensional Simpson quadrature rule.

Algorithm \ref{algo:schwarz} describes our implementation. It uses abbreviations for all MPI functions. Trivial keywords are omitted.
The algorithm exchanges arbitrary values to start the iteration process after assembling the Galerkin-matrix and the load vector. Every iteration consists of the following steps:
\begin{enumerate}
    \item Send own internal values (other ranks boundary values) to neighbours.
    \item Collect boundary values from right and left neighbours.
    \item Calculate the error of the difference of own solution and received values.
    \item Solve the System $Au=\phi$.
\end{enumerate}
Special consideration must be given to the first and the last subdomain, i.e. the ones which lie on the boundary of the original mesh. They are constrained by the boundary condition on the outer boundary, and don't need to send values to the left/right as they have no neighbours on that side.

In order to handle general meshes we also tried to implement an algorithm that recursively splits and redistributes general triangular meshes in a distributed manner based on breadth-first search. This algorithm however was outperformed by simply reading the whole mesh on every core. In the end we did not include the handling of general meshes in our project.

\section{Experimental Results}\label{sec:exp}
\mypar{Experimental setup} We ran our experiments on the Leonhard cluster\footnote{www.scicomp.ethz.ch/wiki/Leonhard} and on one of our own machines. Specifications for the software and hardware used can be found below the results. Additionally, we wrote bash scripts to help us run large tests using different mesh sizes, different numbers of cores and different LU-solvers. Finally, we only used SuperLU \cite{li05} for our results.

We used three different functions for the stiffness coefficient $\sigma$:
\begin{equation}
\sigma_1(x,y) = 1
\end{equation}
\begin{equation}
\sigma_2(x,y) = xy
\end{equation}
\begin{equation}
\sigma_3(x,y) = \sin(\pi x)\sin(\pi y)
\end{equation}
The results were very similar for all three choices of $\sigma$. With this in mind we decided to present only the results for $\sigma_3$. Correspondingly, for the right-hand side load function we simply used $f(x,y) = 1$.

\mypar{Results}
All results were gathered on a square mesh of $\Omega = [0, 1]\times[0,1]$. In all plots the keyword "size" represents the number of quadrilateral cells of the mesh in each direction (x and y). Therefore, for size $= n$ the mesh contains $n \times n$ cells and $(n+1)^2$ vertices.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/total_speedup.pdf}
    \caption{Speedup plot, $T_1/T_P$, baseline nearly 2h, 10 runs. Blue line is linear speedup. Box from 1st to 3rd quartile, whiskers represent 1.5 times IQR.}
    \label{fig:speedup_total}
\end{figure}

Following, we will present briefly our obtained results.

Figure \ref{fig:speedup_total} shows a speedup plot for up to 16 cores. The blue line represents linear speedup. The baseline is the median of 10 runs of the same algorithm on a single core. Our plot shows superlinear speedup for up to 16 cores.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/time_composition_stacked.pdf}
    \caption{Decomposition of the whole implementation, median of 10 runs, error bars indicate min/max.}
    \label{fig:composition_stacked}
\end{figure}


In Figure \ref{fig:composition_stacked} we present the contributions of different parts of the algorithm, measured together with Figure \ref{fig:speedup_total}. It is apparent that the main part is occupied by matrix assembly. The assembly of the load vector and SuperLU (LU-decomposition) takes very little time compared to the assembly of the stiffness matrix and is vanishingly small. For 5 and more cores the iterative part of the Schwarz algorithm starts to dominate.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/time_composition_split.pdf}
    \caption{Decomposition of all iterations, median of 10 runs, error bars indicate min/max.}
    \label{fig:composition_split}
\end{figure}

In Figure \ref{fig:composition_split} we illustrate the contributions to the iteration part (the red bar) of Figure \ref{fig:composition_stacked}. Calculating the solution vector from a given matrix factorization contributes the most to the total iteration time. The communication cost is negligible.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/fenics_combined.pdf}
    \caption{Comparison between FEniCS and TurboFEM, 6 runs. Box from 1st to 3rd quartile, whiskers represent 1.5 times IQR.}
    \label{fig:fenics_comparison}
\end{figure}

In Figure \ref{fig:fenics_comparison} we present a comparison of our approach to FEniCS \cite{AlnaesBlechta2015a}. Both cases were run on a single core, since we were unable to get FEniCS to run in parallel. In particular, the assembly of the Galerkin-matrix is slower than the assembly built into FEniCS. This is due to our unoptimized assembly algorithm. In conclusion we see that FEniCS has superior performance. Because we chose SuperLU, our code was faster in solving the LSE. The other aspects of Algorithm \ref{algo:fem} were slower.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/shared_vs_mpi.pdf}
    \caption{Threads (left) vs. MPI (right), 8 cores, 10 runs. Box from 1st to 3rd quartile, whiskers represent 1.5 times IQR.}
    \label{fig:shared_vs_mpi}
\end{figure}

We implemented two approaches: shared memory with threads and distributed memory using MPI. Algorithm \ref{algo:schwarz} can easily be adapted for shared memory, since the approach is the same. In Figure \ref{fig:shared_vs_mpi} we present a runtime comparison between the two approaches. Both implementations scale roughly equally. It turns out that the main contribution is the assembly of the Galerkin-matrix. This is common to both implementations.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/strong_scaling.pdf}
    \caption{Strong scaling for more than 16 cores for problem size 500, baseline 2h, 10 runs. Blue line is linear speedup. Box from 1st to 3rd quartile, whiskers represent 1.5 times IQR.}
    \label{fig:strong_scaling}
\end{figure}

In Figure \ref{fig:strong_scaling} we show strong scaling for more than 16 cores. The baseline was two hours, the same as in Figure \ref{fig:speedup_total}. The blue line represents linear speedup. The code scales nearly linearly for up to 64 cores. We achieve superlinear speedup up to 16 cores. This can be seen in Figure \ref{fig:speedup_total}. We saw a linear increase from 75'000 to 576'000 iterations needed to converge (16 and 128 cores, respectively). We suspect that this is due to using the sum of all the local errors when checking for convergence. Naturally, the more subdomains used the larger the sum of the error will be, thus taking longer to converge. Some insight was gained with a runtime decomposition similar to Figure \ref{fig:composition_stacked}. It showed that most of the time was spent in assembly of the stiffness matrix, next followed SuperLU (LU-factorization). All other timings (time spent iterating) scaled roughly linearly up to 64 cores. This can be seen in Figure \ref{fig:strong_scaling} too. We requested 18 cores per node, hence for up to 18 cores, communication is only on a single node. No network communication is needed. With 19 and more cores, the nodes must communicate over network. However, a cursory glance at the numbers showed that the communication cost is still negligible as SuperLU and applying boundary conditions don't scale as strongly as assembly for more than 24 cores and thus start to dominate the execution time. This could explain the drop in performance for 24 and more cores, however, more investigation is needed.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/scaling.pdf}
    \caption{log-log plot of sequential runtime of assembly, SuperLU and applying boundary conditions vs. the number of cells in the mesh, 1 core, median of 10 runs, error bars indicate min/max. The dashed line shows quadratic scaling.}
    \label{fig:assembly-scaling}
\end{figure}

\mypar{Runtime model} In order to explain the superlinear speedup for up to 16 cores we created a simple model for the runtime based on the experimental data.

We have shown in Figure \ref{fig:composition_stacked} that the largest part of the execution time is spent on assembly and on iterating. For this reason, we consider only those contributions and measured how our assembly algorithm scales with the problem size. Figure \ref{fig:assembly-scaling} demonstrates that assembly scales roughly quadratically with the number of cells in the mesh. In other words, splitting the mesh into two subdomains cuts the assembly time by a factor of four. For simplicity sake and based on Figure \ref{fig:composition_split} we assume a roughly constant iteration time independent of the number of cores.

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/model_runtime.pdf}
    \caption{Runtime model and measured data (median of 10 runs)}
    \label{fig:runtime-model}
\end{figure}

We now introduce following runtime model. For large enough problem sizes, the total runtime is approximated by
\begin{equation}
    \label{eq:runtime-model}
    t_{total}(p) = 
    \begin{cases}
        t_{assembly}(1) & \text{if $p=1$}\\
        \frac{t_{assembly}(1)}{p^2} + t_{iteration} & \text{otherwise}
    \end{cases}
\end{equation}
where $p$ is the number of processors and with $t_{assembly}(1) = 7'400$s and $t_{iteration} = 0.28$s. This gives a fairly good approximation, as seen in Figure \ref{fig:runtime-model} which uses the same data as Figure \ref{fig:speedup_total}. The resulting speedup (for $p>1$) is given by
\begin{equation}
    \label{eq:speedup-model}
    s(p) = \frac{t_{total}(1)}{t_{total}(p)}
         = \frac{p^2 t_{assembly}(1)}{t_{assembly}(1) + p^2 t_{iteration}}
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=1.0\columnwidth]{images/model_speedup.pdf}
    \caption{Predicted speedup and measured data (median of 10 runs)}
    \label{fig:speedup-model}
\end{figure}

The predicted speedup is plotted in Figure \ref{fig:speedup-model}. It also displays superlinear speedup for small enough $p$, which shows that the quadratic scaling of the assembly algorithm leads to superlinear speedup.

\mypar{Specifications for Figure \ref{fig:speedup_total}, \ref{fig:composition_stacked}, \ref{fig:composition_split}, \ref{fig:shared_vs_mpi}, \ref{fig:strong_scaling}, \ref{fig:assembly-scaling}, \ref{fig:runtime-model}, \ref{fig:speedup-model}} run on Leonhard, gcc 6.3.0, cmake RELEASE, Eigen 3.3.3, openMPI 4.0.1, SuperLU 5.2.1, openBLAS 0.2.19, Processor: Xeon Gold 6140, max. 3.7GHz, 18 cores, L1: 1.125MiB, L2: 18MiB, L3: 24.75MiB

\mypar{Specifications for Figure \ref{fig:fenics_comparison}} single core, gcc 9.2.0, cmake RELEASE, Python 3.8.1, FEniCS 2019.1.0, UFC backend 2018.1.0, SuperLU 6.2.0, Eigen 3.3.7, blas 3.9.0, openMPI 4.0.2, Processor: i7-7820HQ, max. 2.90GHz, 8 cores, L1: 256KiB, L2: 1MiB, L3: 8Mib, 64bit aligned, Kernel: 4.19.91-1-MANJARO

\section{Conclusions}
We showed how a parallel implementation of a Schwarz Method can achieve superlinear speedup. We showed, that both memory approaches (shared memory and distributed memory) scale roughly equally.

Most time spent for the sequential algorithm is in the assembly of the stiffness matrix. By working with smaller meshes in parallel this can be sped up significantly thanks to the quadratic scaling of the assembly algorithm. Our algorithm scales really well up to 64 cores. With even more cores, the speedup is no longer close to the optimal linear speedup.

Furthermore, we showed that our algorithm can hold with industrial standards to a certain extent. Our computed runtime model shows that the time mostly depends on assembling and iteration. Other aspects were neglectable.

Nevertheless, we have some ideas how our implementation can be improved. In particular, for larger numbers of cores the convergence rate of the algorithm could be improved, for instance by using a better metric for the error, by using a larger overlap and by using more sophisticated boundary conditions such as Robin boundary conditions \cite{MR3450068}. In addition, our assembly algorithm and the code to apply boundary conditions can be significantly optimized.

% References should be produced using the bibtex program from suitable
% BiBTeX files (here: bibl_conf). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{bibl_conf}

\end{document}

