import os
import time
import matplotlib.pyplot as plt
import numpy as np

gridres = [x for x in range(10,100,10)]
iterations = [x for x in range(50,100,10)]

xx,yy = np.meshgrid(gridres, iterations)

#z = np.zeros((len(gridres), len(iterations)))

"""for g in range(len(gridres)):
    for i in range(len(iterations)):
        start = time.time()
        ret = os.popen("echo \"{} {}\" | ./build/turbofem/serial_heat".format(int(iterations[i]), int(gridres[g]))).read()
        end = time.time()
        z[g,i] = end-start
        print(end-start)
"""


#np.savetxt("data.txt",z)
z = np.loadtxt("data.txt")


for i,x in enumerate(iterations):
    plt.plot(gridres,z[:,i],label=str(x))

#plt.yticks(iterations, iterations)
plt.ylabel("Time [s]")
plt.xticks(gridres,gridres)
plt.xlabel("Grid Resolution")

plt.legend()

plt.title("Computing Time for various iteration amounts")

#plt.colorbar()
plt.show()
#plt.savefig("plot_it_vs_gridres.png")