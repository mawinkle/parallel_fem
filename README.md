# TurboFEM
[![pipeline status](https://gitlab.ethz.ch/mawinkle/parallel_fem/badges/master/pipeline.svg)](https://gitlab.ethz.ch/mawinkle/parallel_fem/commits/master)

The big parallel FEM, on its way to defeat all the other FEM libraries, especially
[Lehrfem++](https://github.com/craffael/lehrfempp "A clearly inferior FEM library").

## Dependencies
```
apt-get update && apt-get -y install cmake make autoconf libeigen3-dev xorg-dev libglu1-mesa-dev
git submodule sync --recursive
git submodule update --init --recursive
```

## Run
```mkdir build; cd build; cmake ..; make```

## GUI
The GUI is built using ```nanogui```, and will run everything. It can be found inside the ```app```-subdirectory.

## Report
The report can be found in the ```report```-subdirectory.

## Proposal
The proposal can be found in the ```report```-subdirectory.