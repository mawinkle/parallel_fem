#!/bin/sh

# Required global variables:
# - CI_JOB_ID           : The number of the current build.
# - CI_COMMIT_TITLE     : The commit that the current build is testing.
# - GH_REPO_NAME        : The name of the repository.
# - GH_REPO_REF         : The GitHub reference to the repository.
# - GH_REPO_TOKEN       : Secure token to the github repository.

set -e

cd ..
mkdir -p code_docs
cd code_docs

rm -rf $GH_REPO_NAME

# Get the current master branch
git clone -b master https://git@$GH_REPO_REF
cd $GH_REPO_NAME

git config --global push.default simple
# Pretend to be an user called Travis CI.
git config user.name "Gitlab CI"
git config user.email "ci@gitlab.ethz.ch"
rm -rf *

# move back to repo
cd ../../parallel_fem

mkdir -p build
cd build;
cmake .. -DENABLE_DOCUMENTATION=ON
make doc_doxygen

# copy documentation to code_docs folder
cp -r doc/html/* ../../code_docs/$GH_REPO_NAME

cd ../../code_docs/$GH_REPO_NAME

echo 'Uploading documentation to the master branch...'
git add --all

git commit -m "Deploy code docs to GitHub Pages: ${CI_JOB_ID}" -m "Commit: ${CI_COMMIT_TITLE}"

git push --force "https://${GH_REPO_TOKEN}@${GH_REPO_REF}"