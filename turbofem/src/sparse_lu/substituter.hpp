#ifndef SUBSTITUTER_HPP
#define SUBSTITUTER_HPP
#include <utility>
#include <cstddef>
#include <algorithm>
#include <unordered_map>
#include <vector>
template<typename value_type>
struct ascending_comparator{
    bool operator()(const std::pair<size_t, value_type>& p1, const std::pair<size_t, value_type>& p2) const{
        return p1.first < p2.first;
    }
};
template<typename value_type>
struct descending_comparator{
    bool operator()(const std::pair<size_t, value_type>& p1, const std::pair<size_t, value_type>& p2) const{
        return p2.first < p1.first;
    }
};
template <typename _scalar> struct sparse_lu;
template<typename _scalar>
struct lu_substituter{
    using scalar = _scalar;
    using size_t = std::size_t;
    std::vector<std::pair<std::pair<size_t, size_t>, scalar>> L;
    std::vector<std::pair<std::pair<size_t, size_t>, scalar>> R;
    std::vector<std::pair<size_t, size_t>> P;
    lu_substituter(const sparse_lu<scalar>& lu){
        std::vector<std::pair<size_t, std::vector<std::pair<size_t, scalar>>>> L_temp;
        for(auto it = lu.L.entries.begin();it != lu.L.entries.end();it++){
            L_temp.push_back(std::make_pair(it->first, std::vector<std::pair<size_t, scalar>>()));
            for(auto it2 = it->second.begin();it2 != it->second.end();it2++){
                L_temp.back().second.push_back(std::make_pair(it2->first, it2->second));
            }
        }
        std::sort(L_temp.begin(), L_temp.end(), ascending_comparator<std::vector<std::pair<size_t, scalar>>>());
		std::vector<std::pair<size_t, scalar>> rowbuffer;
		size_t ascdesc = 0;
		
		for(auto it = L_temp.begin(); it != L_temp.end();it++){
			rowbuffer.clear();
			size_t row_index = it->first;
			
			for(auto it2 = it->second.begin();it2 != it->second.end();it2++){
				rowbuffer.push_back(*it2);
			}
			
			ascdesc++;
			if(ascdesc & 1)
				std::sort(rowbuffer.begin(), rowbuffer.end(), descending_comparator<scalar>());
			else
				std::sort(rowbuffer.begin(), rowbuffer.end(), ascending_comparator<scalar>());
			for(auto it2 = rowbuffer.begin();it2 != rowbuffer.end();it2++){
				L.push_back(std::make_pair(std::make_pair(row_index, it2->first), it2->second));
			}
		}
		//std::cout << lu.L.toDense() << "\n\n";
		for(auto it = L.begin();it != L.end();it++){
			//std::cout << (*it).first.first << ", " << (*it).first.second << ", " << (*it).second << std::endl;
		}
    }
};
#endif