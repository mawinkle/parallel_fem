#ifndef HASH_MATRIX_HPP
#define HASH_MATRIX_HPP
#include <cstddef>
#include <unordered_map>
#include <cassert>
#include <iostream>
#include <tuple>
#include <vector>
#include <Eigen/Dense>
#include "../exceptions.hpp"


enum major : int{
    row_major, col_major
};
template<major major_type>
struct major_index{};
template<>
struct major_index<row_major>{
    constexpr size_t operator()(size_t i, size_t j){
        return i;
    }
};
template<>
struct major_index<col_major>{
    constexpr size_t operator()(size_t i, size_t j){
        return j;
    }
};
template<major major_type>
struct minor_index{};
template<>
struct minor_index<row_major>{
    constexpr size_t operator()(size_t i, size_t j){
        return j;
    }
};
template<>
struct minor_index<col_major>{
    constexpr size_t operator()(size_t i, size_t j){
        return i;
    }
};
template<typename _scalar, major _majorness = row_major>
struct hash_matrix{
    constexpr static major majorness = _majorness;
    using scalar = _scalar;
    using size_t = std::size_t;
    size_t m, n;
    
    hash_matrix(const Eigen::MatrixXd& matrix) {
        m = matrix.rows();
        n = matrix.cols();

        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                coeff_ref(i, j) = matrix(i, j);
            }
        }
    }

    hash_matrix(size_t _m, size_t _n) : m(_m), n(_n){

    }
	
    std::unordered_map<size_t, std::unordered_map<size_t, scalar>> entries;
    scalar& coeff_ref(size_t i, size_t j){
        assert(i < m && j < n);
        auto& antries = entries;
        if(entries.find(major_index<majorness>()(i,j)) == entries.end()){
            entries.insert(std::make_pair(major_index<majorness>()(i,j), std::unordered_map<size_t , scalar>()));
        }
        
        if(entries[major_index<majorness>()(i,j)].find(minor_index<majorness>()(i,j)) == entries[major_index<majorness>()(i,j)].end()){
            entries[major_index<majorness>()(i,j)].insert(std::make_pair(minor_index<majorness>()(i,j), 0));
        }
        return entries[major_index<majorness>()(i,j)][minor_index<majorness>()(i,j)];
    }
    bool has(size_t i, size_t j){
        assert(i < m && j < n);
        if(entries.find(major_index<majorness>()(i,j)) == entries.end())return false;
        if(entries[major_index<majorness>()(i,j)].find(minor_index<majorness>()(i,j)) == entries[minor_index<majorness>()(i,j)].end())return false;
        return true;
    }
    void erase(size_t i, size_t j){
        assert(i < m && j < n);
        if(has(major_index<majorness>()(i,j),minor_index<majorness>()(i,j))){
            entries[major_index<majorness>()(i,j)].erase(minor_index<majorness>()(i,j));
        }
    }
    void set_id_major_vector(size_t i){
        assert(i < m);
        if(entries.find(i) == entries.end())
            entries.insert(std::make_pair(i, std::unordered_map<size_t , scalar>()));
        else
            entries[i].clear();
        entries[i].insert(std::make_pair(i, 1));
    }
	bool add_row(size_t from, size_t to, const scalar& coeff, std::vector<std::tuple<size_t, size_t, scalar>>& updates, scalar epsilon = 0){
		using std::abs;
		assert(majorness == row_major);
		if(entries.find(from) == entries.end())return false;
		auto& map = entries[from];
		for(auto& pair : map){
            //std::cout << toDense() << "\n";
            //double x1 = coeff_ref(to, pair.first);
            //std::cout << toDense() << "\n";
			coeff_ref(to, pair.first) += pair.second * coeff;
            //double x2 = coeff_ref(to, pair.first);
			if(abs(coeff_ref(to, pair.first)) <= epsilon){
				entries[to].erase(pair.first);
				updates.push_back(std::make_tuple(to, pair.first, 0));
			}
			else
				updates.push_back(std::make_tuple(to, pair.first, coeff_ref(to, pair.first)));
		}
		return true;
	}
	void permute_rows(size_t i, size_t j){
		if(entries.find(i) == entries.end() && entries.find(j) == entries.end())return;
		if(entries.find(i) == entries.end()){
			entries.insert(std::make_pair(i, std::move(entries[j])));
			entries.erase(j);
			return;
		}
		if(entries.find(j) == entries.end()){
			entries.insert(std::make_pair(j, std::move(entries[i])));
			entries.erase(i);
			return;
		}
		std::swap(entries[i], entries[j]);
	}
	Eigen::Matrix<scalar, Eigen::Dynamic, Eigen::Dynamic> toDense() const {
        Eigen::Matrix<scalar, Eigen::Dynamic, Eigen::Dynamic> ret = Eigen::Matrix<scalar, Eigen::Dynamic, Eigen::Dynamic>::Zero(m, n);
        for(const std::pair<size_t, std::unordered_map<size_t, scalar>>& map : entries){
            for(const std::pair<size_t, scalar>& entryp : map.second){
                assert(map.first < m && "map.first in range");
                assert(entryp.first < n && "entryp.first in range");
                ret(map.first, entryp.first) = entryp.second;
            }
        }
        if(majorness == row_major){
            return ret;
        }
        return ret.transpose();
    }
    friend std::ostream& operator<<(std::ostream& ost, const hash_matrix<scalar, majorness>& o){
        if constexpr(o.majorness == col_major){
            ost << "Columns: \n";
        }
        if constexpr(o.majorness == row_major){
            ost << "Rows: \n";
        }
        for(auto it = o.entries.begin();it != o.entries.end();it++){
            // std::cout << it->first << ": ";
            for(auto it2 = (it->second).begin();it2 != (it->second).end();it2++){
                ost << "(" << it2->first << ", " << it2->second << "), ";
            }
            ost << "\n";
        }
        return ost;
    }
};
#endif
