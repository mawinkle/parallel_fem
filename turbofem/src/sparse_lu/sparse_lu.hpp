#ifndef SPARSE_LU_HPP
#define SPARSE_LU_HPP
#include "hash_matrix.hpp"
#include <cassert>
#include <tuple>
#include <utility>
#include <limits>
#include "substituter.hpp"

#include "../exceptions.hpp"

/**
 * @brief Returns LU decomposition of matrix A, where $PA = LU$
 */
template <typename _scalar> struct sparse_lu {
	using scalar = _scalar;
	hash_matrix<scalar, row_major> U;
	hash_matrix<scalar, col_major> U_col_maj;
	hash_matrix<scalar, row_major> L;
	hash_matrix<scalar, row_major> P; ///< P*A = LU
	scalar epsilon;
	sparse_lu(const hash_matrix<scalar, row_major> &mat, scalar eps = std::numeric_limits<scalar>::epsilon() * 4) : U(mat), U_col_maj(U.m, U.n), L(U.m, U.n), P(U.m, U.m), epsilon(eps) {
		for (auto &p1 : mat.entries) {
			for (auto &p2 : p1.second) {
				U_col_maj.coeff_ref(p1.first, p2.first) = p2.second;
			}
		}
		for(size_t i = 0;i < P.m;i++){
			P.coeff_ref(i,i) = 1;
		}
		compute();
		// comment Matthias: already done in compute()
		/*for(size_t i = 0;i < L.m;i++){
			L.coeff_ref(i,i) = 1;
		}*/
		lu_substituter<scalar> lus(*this);
	}
	scalar pivotize(size_t column){
		std::unordered_map<size_t, scalar> column_map = U_col_maj.entries[column];
		bool swap_needed = false;
		scalar max_abs_value = 0;
		size_t index_max = -1;
		for(auto& entry : column_map){
			if(entry.first >= column){
				if(abs(entry.second) > max_abs_value){
					max_abs_value = abs(entry.second);
					index_max = entry.first;
				}
			}
		}
		if(max_abs_value == 0){
			// std::cout << column << "\n\n";
			//assert(false && "Matrix singular");
			throw MatrixSingularException();
		}
		swap_needed = index_max != column;
		if(swap_needed){//Swap rows [column] and [index_max]
			for(auto entry : U.entries[column]){
				U_col_maj.entries[entry.first].erase(column);
			}
			for(auto entry : U.entries[index_max]){
				U_col_maj.entries[entry.first].erase(index_max);
			}
			U.permute_rows(column, index_max);
			L.permute_rows(column, index_max);
			P.permute_rows(column, index_max);
			for(auto entry : U.entries[column]){
				U_col_maj.coeff_ref(column, entry.first) = entry.second;
			}
			for(auto entry : U.entries[index_max]){
				U_col_maj.coeff_ref(index_max, entry.first) = entry.second;
			}
		}
		assert(U.toDense() == U_col_maj.toDense());// Assert that the matrices are still the same.
		return U.coeff_ref(column, column);
	}
	void get_alphas(size_t column,const scalar& pivot, std::vector<std::pair<size_t, scalar>>& alphas){
		std::unordered_map<size_t, scalar> column_map = U_col_maj.entries[column];
		for(auto& entry : column_map){
			if(entry.first > column){
				alphas.push_back(std::make_pair(entry.first, -entry.second / pivot));
			}
		}
	}
	void compute(){
		using std::abs;
		std::vector<std::pair<size_t, scalar>> alphas;
		std::vector<std::tuple<size_t, size_t, scalar>> updates;
		updates.reserve(U.n);
		alphas.reserve(U.m);
		// std::cout << U.toDense() << "\n";
		for(size_t column = 0;column < U.n;column++){
			scalar pivot = pivotize(column);
			get_alphas(column, pivot,  alphas);
			for(auto& elimination : alphas){
				L.coeff_ref(elimination.first, column) = -elimination.second;
				updates.clear();
				U.add_row(column, elimination.first, elimination.second, updates, 0.00001);
				for(const std::tuple<size_t, size_t, scalar>& update : updates){
					if(std::get<2>(update) == 0){
						U_col_maj.entries[std::get<1>(update)].erase(std::get<0>(update));
					}
					else{
						U_col_maj.coeff_ref(std::get<0>(update), std::get<1>(update)) = std::get<2>(update);
					}
				}
				assert(U.toDense() == U_col_maj.toDense());
				updates.clear();
			}
			alphas.clear();
		}
		for(size_t i = 0;i < L.n;i++){
			L.coeff_ref(i,i) = 1;
		}
		
	}
};
#endif /* SPARSE_LU_HPP */