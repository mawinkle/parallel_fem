#include <turbofem/src/mpi/partitioning/all.hpp>
#include <iostream>
int main()
{
    turbofem::partitioning::MeshBuilder<double> builder(31, 31, 0, 0, 1, 1);
    turbofem::partitioning::Mesh<double> mesh = builder.createMesh();

    std::cout << mesh.getVertexCount() << " " << mesh.getEdgeCount() << " " << mesh.getCellCount() << std::endl;

    for(int i = 0; i < mesh.getVertexCount(); i++) {
        std::cout << i << " " << mesh.getPosition(i).first << " " << mesh.getPosition(i).second << std::endl;
    }

    for(int i = 0; i < mesh.getEdgeCount(); i++) {
        std::cout << i << " " << mesh.getEdgePoints(i).first << " " << mesh.getEdgePoints(i).second << std::endl;
    }
    for(int i = 0; i < mesh.getCellCount(); i++) {
        std::cout << i << " " << std::get<0>(mesh.getCellPoints(i)) << " " << std::get<1>(mesh.getCellPoints(i))<< " " << std::get<2>(mesh.getCellPoints(i)) << std::endl;
    }
    return 0;
}
