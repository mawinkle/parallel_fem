#ifndef TURBOFEM_EXCEPTIONS_HPP
#define TURBOFEM_EXCEPTIONS_HPP

#include <exception>

struct MatrixSingularException : public std::exception {
    const char* what() const throw() {
        return "Matrix singular";
    }
};

#endif /* TURBOFEM_EXCEPTIONS_HPP */