#ifndef MPI_PARTITIONING_MESHBUILDER_HPP
#define MPI_PARTITIONING_MESHBUILDER_HPP

#include <vector>

namespace turbofem
{

namespace partitioning
{

/**
 * @brief Class to represent a mesh builder
 * 
 * @tparam T what type to use for positioning the points
 */
template <typename T>
class MeshBuilder
{
public:
    /**
     * @brief Construct a new Mesh Builder object
     * 
     */
    MeshBuilder() = delete;

    /**
     * @brief Construct a new Mesh Builder object
     * 
     * @param gridX how many cells it should have in x direction
     * @param gridY how many cells it should have in y direction
     * @param x_from minimum value of x
     * @param y_from minimum value of y
     * @param x_to maximum value of x
     * @param y_to maximum value of y
     */
    MeshBuilder(size_t gridX, size_t gridY, T x_from = T(0), T y_from = T(0), T x_to = T(1), T y_to = T(1)) : gridX_(gridX), gridY_(gridY), x_from_(x_from), x_to_(x_to), y_from_(y_from), y_to_(y_to)
    {
    }

    Mesh<T> createMesh()
    {
        Mesh<T> mesh;
        std::vector<std::vector<size_t>> vertex_table(gridX_ + 1, std::vector<size_t>(gridY_ + 1, -1));

        for (size_t i = 0; i <= gridX_; i++)
        {
            for (size_t j = 0; j <= gridY_; j++)
            {
                vertex_table[i][j] = mesh.addVertex((x_to_ - x_from_) * i / gridX_ + x_from_, (y_to_ - y_from_) * j / gridY_ + y_from_);
                std::pair<double, double> pos = mesh.getPosition(vertex_table[i][j]);
            }
        }

        for (size_t i = 0; i <= gridX_; i++)
        {
            for (size_t j = 0; j <= gridY_; j++)
            {
                if (i < gridX_ && j < gridY_)
                {
                    mesh.addEdge(vertex_table[i][j], vertex_table[i + 1][j]);     // x dir
                    mesh.addEdge(vertex_table[i][j], vertex_table[i][j + 1]);     // y dir
                    mesh.addEdge(vertex_table[i][j], vertex_table[i + 1][j + 1]); // diagonal

                    mesh.addCell(vertex_table[i][j], vertex_table[i][j + 1], vertex_table[i + 1][j + 1]); // upper triangles
                    mesh.addCell(vertex_table[i][j], vertex_table[i + 1][j], vertex_table[i + 1][j + 1]); // lower triangle
                }
                else if (i < gridX_)
                {
                    mesh.addEdge(vertex_table[i][j], vertex_table[i + 1][j]);
                }
                else if (j < gridY_)
                {
                    mesh.addEdge(vertex_table[i][j], vertex_table[i][j+1]);
                }
            }
        }
        return mesh;
    }

private:
    size_t gridX_, gridY_;
    T x_from_, x_to_, y_from_, y_to_;
};

} // namespace partitioning
} // namespace turbofem

#endif