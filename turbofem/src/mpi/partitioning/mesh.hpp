#ifndef MPI_MESH_HPP
#define MPI_MESH_HPP

#include <mpi.h>
#include <vector> // to be replaced later
#include <iostream>
#include <cassert>

namespace turbofem
{
namespace partitioning
{
/**
 * @brief Mesh class
 * 
 * @tparam T type of coordinate system
 */
template <typename T>
class Mesh
{
public:
    using index_type = size_t;
    /**
     * @brief Construct a new Mesh object
     * 
     */
    Mesh() = default;

    size_t getVertexCount() const { return vertices_.size(); }
    size_t getEdgeCount() const { return edges_.size(); }
    size_t getCellCount() const { return cells_.size(); }

    /**
     * @brief Add a vertex to the mesh
     * 
     * @param x x coordinate
     * @param y y coordinate
     * @return size_t index of vertex inside this mesh
     */
    index_type addVertex(T x, T y)
    {
        index_type currSize = vertices_.size();
        vertices_.push_back({currSize, {x, y}});
        return currSize;
    }

    /**
     * @brief Get the position of a vertex
     * 
     * @param index vertex-index
     * @return std::pair with x and y
     */
    std::pair<T, T> getPosition(index_type index) const
    {
        assert(index < vertices_.size());
        for (auto &v : vertices_)
        {
            if (v.first == index)
            {
                return v.second;
            }
        }
        return std::make_pair<T, T>(0, 0);
    }

    /**
     * @brief Add an edge to the mesh
     * 
     * @param from index of vertex
     * @param to index of vertex
     * @return index_type index of edge inside this mesh 
     */
    index_type addEdge(index_type from, index_type to)
    {
        index_type currSize = edges_.size();
        edges_.push_back({currSize, {from, to}});
        return currSize;
    }
    /**
     * @brief Add a cell to the mesh
     * 
     * @param v1 index of vertex 1
     * @param v2 index of vertex 2
     * @param v3 index of vertex 3
     * @return index_type index of cell inside the mesh
     */
    index_type addCell(index_type v1, index_type v2, index_type v3)
    {
        index_type currSize = cells_.size();
        cells_.push_back({currSize, {v1, v2, v3}});
        return currSize;
    }

    std::pair<index_type, index_type> getEdgePoints(index_type i)
    {
        for (auto &e : edges_)
        {
            if (e.first == i)
            {

                return {e.second.first, e.second.second};
            }
        }
        return {};
    }

    std::tuple<index_type, index_type, index_type> getCellPoints(index_type i)
    {
        for (auto &e : cells_)
        {
            if (e.first == i)
            {
                return e.second;
            }
        }
        return {};
    }

private:
    std::vector<std::pair<index_type, std::pair<T, T>>> vertices_;                             ///< container of (index_type, (x,y))
    std::vector<std::pair<index_type, std::pair<index_type, index_type>>> edges_;              ///< container of (index_type, (from, to))
    std::vector<std::pair<index_type, std::tuple<index_type, index_type, index_type>>> cells_; ///< container of (index_type, (v1, v2, v3))
};

} // namespace partitioning
} // namespace turbofem

template <typename T>
std::ostream &operator<<(std::ostream &os, const turbofem::partitioning::Mesh<T> &m)
{
    os << "Vertices: " << m.getVertexCount() << std::endl;
    for (int i = 0; i < m.getVertexCount(); i++)
    {
        os << m.getPosition(i).first << "/" << m.getPosition(i).second << " ";
    }
    os << std::endl;
    os << "Edges: " << m.getEdgeCount() << std::endl;
    os << "Cells: " << m.getCellCount() << std::endl;
    return os;
}

#endif