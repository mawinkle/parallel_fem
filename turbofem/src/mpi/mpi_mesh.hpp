#ifndef MPI_MESH_HPP
#define MPI_MESH_HPP

#include <fstream>
#include <iostream>

struct mpi_mesh
{
    // amount of entries in each array
    int es;
    int vs;
    int cs;
    // vertices
    int *v_index;
    double *vx; // x position
    double *vy; // y position
    // edges
    int *e_index;
    int *e1;
    int *e2;
    // cells
    int *c_index;
    int *c1;
    int *c2;
    int *c3;
};

void readSizeFromFile(const char *filename, int *vs, int *es, int *cs)
{
    std::ifstream file(filename);
    if(!file.is_open()) {
        std::cerr << "File could not be opened." << std::endl;
        return;
    }
    file >> *vs >> *es >> *cs; // read numbers
    file.close();
}

void readMeshFromFile(const char *filename, mpi_mesh *m)
{
    std::ifstream file(filename);
    file >> m->vs >> m->es >> m->cs; // read numbers
    // allocate arrays
    m->vx = new double[m->vs];
    m->vy = new double[m->vs];
    m->v_index = new int[m->vs];
    m->e1 = new int[m->es];
    m->e2 = new int[m->es];
    m->e_index = new int[m->es];
    m->c1 = new int[m->cs];
    m->c2 = new int[m->cs];
    m->c3 = new int[m->cs];
    m->c_index = new int[m->cs];

    // read from file all vertices
    for (int i = 0; i < m->vs; i++)
    {
        file >> m->v_index[i] >> m->vx[i] >> m->vy[i];
    }
    // read edges
    for (int i = 0; i < m->es; i++)
    {
        file >> m->e_index[i] >> m->e1[i] >> m->e2[i];
    }
    // read cells
    for (int i = 0; i < m->cs; i++)
    {
        file >> m->c_index[i] >> m->c1[i] >> m->c2[i] >> m->c3[i];
    }
}

#endif