#ifndef MESH_HPP
#define MESH_HPP

#include "vector2.hpp"
#include <array>
#include <memory>
#include <unordered_set>
#include <vector>
#include <unordered_map>
#include <type_traits>
#include <iostream>

/**
 * @brief Clone of pair
 * 
 * @tparam T type of entry
 */
template <typename T>
using pear = std::tuple<T, T>;
/**
 * @brief TurboFEM-Triple
 * 
 * @tparam T type of entry
 */
template <typename T>
using triple = std::tuple<T, T, T>;

/**
 * @brief whatever this does
 * 
 * @param x some ID maybe?
 * @param k some shifting
 * @return std::uint64_t some rotational stuff done here
 */
static inline std::uint64_t rotl_(const std::uint64_t x, int k)
{
    return (x << k) | (x >> (64 - k));
}

namespace std
{
/**
     * @brief hash struct (functor)
     * 
     * @tparam T type of entry
     */
template <typename T>
struct hash<pear<T>>
{
    /**
     * @brief generate a hash for a pair
     * 
     * @param p pair
     * @return size_t 
     */
    inline size_t operator()(const pear<T> &p) const
    {
        if constexpr (std::is_integral<T>::value)
        {
            return std::get<0>(p) ^ rotl_(std::get<1>(p), 32);
        }
        if constexpr (!std::is_integral<T>::value)
        {
            return hash<decltype(std::get<0>(p))>()(std::get<0>(p)) ^ rotl_(hash<decltype(std::get<1>(p))>()(std::get<1>(p)), 32);
        }
    }
};
/**
 * @brief hash struct (Functor)
 * 
 * @tparam T type of entry
 */
template <typename T>
struct hash<triple<T>>
{
    /**
     * @brief generate a hash for a triple
     * 
     * @param t triple
     * @return size_t 
     */
    inline size_t operator()(const triple<T> &t) const
    {
        if constexpr (std::is_integral<T>::value)
        {
            return std::get<0>(t) ^ rotl_(std::get<1>(t), 24) ^ rotl_(std::get<2>(t), 48);
        }
        if constexpr (!std::is_integral<T>::value)
        {
            return hash<decltype(std::get<0>(t))>()(std::get<0>(t)) ^ rotl_(hash<decltype(std::get<1>(t))>()(std::get<1>(t)), 24) ^ rotl_(hash<decltype(std::get<2>(t))>()(std::get<2>(t)), 48);
        }
    }
};
} // namespace std

template <typename _scalar>
struct mesh;

/**
 * @brief Builds a mesh from vertex, edges and triangles
 * 
 * @tparam _scalar type of positions (mostly double or float)
 */
template <typename _scalar>
struct mesh_builder
{
    using scalar = _scalar;
    std::vector<vector2<scalar>> vertices;    ///< vector of vertices, holding positions
    std::unordered_set<pear<size_t>> edges;   ///< a set of pairs -> edges
    std::unordered_set<triple<size_t>> cells; ///< a set of triples -> cells
    /**
     * @brief Add a vertex to the mesh
     * 
     * @param pos The position of the vertex
     * @return size_t how many vertices are there
     */
    size_t add_vertex(vector2<scalar> pos)
    {
        vertices.push_back(std::move(pos));
        return vertices.size() - 1;
    }
    /**
     * @brief Add a vertex at given position
     * 
     * @param x coordinate 1
     * @param y coordinate 2
     * @return size_t how many vertices are there
     */
    size_t add_vertex(scalar x, scalar y)
    {
        vector2<scalar> pos;
        pos(0) = std::move(x);
        pos(1) = std::move(y);
        vertices.push_back(std::move(pos));
        return vertices.size() - 1;
    }
    /**
     * @brief add an edge from index1 to index2
     * 
     * @param index1 index of first entry of edge
     * @param index2 index of second entry of edge
     */
    void add_edge(size_t index1, size_t index2)
    {
        assert(index1 != index2);
        if (index2 < index1)
            std::swap(index1, index2);
        edges.insert(std::make_tuple(index1, index2));
    }
    /**
     * @brief add a cell with 3 vertices
     * 
     * @param index1 point 1
     * @param index2 point 2
     * @param index3 point 3
     */
    void add_cell(size_t index1, size_t index2, size_t index3)
    {
        size_t i[3] = {index1, index2, index3};
        std::sort(i, i + 3);
        index1 = i[0];
        index2 = i[1];
        index3 = i[2];
        edges.insert(std::make_pair(index1, index2));
        edges.insert(std::make_pair(index2, index3));
        edges.insert(std::make_pair(index1, index3));

        cells.insert(std::make_tuple(index1, index2, index3));
    }
    inline mesh<scalar> get_mesh();
};
template <typename _scalar>
struct vertex;
template <typename _scalar>
struct edge;
template <typename _scalar>
struct triangle;


/**
 * @brief stores vertex information
 * 
 * @tparam _scalar type of entry, mostly double or float
 */
template <typename _scalar>
struct vertex
{
    using scalar = _scalar;
    std::vector<const edge<scalar> *> connected_edges;     ///< which vertices are connected to this one
    std::vector<const triangle<scalar> *> connected_cells; ///< which cells are connected to this one
    vector2<scalar> pos;                                   ///< position inside world
    size_t index;                                          ///< my index inside the mesh
    /**
     * @brief Construct a new vertex object
     * 
     * @param _pos the position of this vertex
     * @param i it's index
     */
    vertex(vector2<scalar> _pos, size_t i) : pos(std::move(_pos)), index(i) {}
    /**
     * @brief Construct a new vertex object
     * 
     * @param x position corrd1
     * @param y position coord2
     * @param i it's index
     */
    vertex(scalar x, scalar y, size_t i) : pos(std::move(x), std::move(y)), index(i) {}
};

enum EDGE_ON_BORDER { NONE = 0, TOP = 1, BOTTOM = 2, LEFT = 4, RIGHT = 8};

/**
 * @brief edge struct
 * 
 * @tparam _scalar type of entry, mostly double or float
 */
template <typename _scalar>
struct edge
{
    int tag = EDGE_ON_BORDER::NONE;

    using scalar = _scalar;
    std::array<const vertex<scalar> *, 2> vertices;       ///< which vertices are used to build this edge
    std::vector<const triangle<scalar> *> adjacent_faces; ///< which faces are adjacent
    /**
     * @brief Construct a new edge object
     * 
     * @param first pointer to vertex 1
     * @param second pointer to vertex 2
     */
    edge(const vertex<scalar> *first, const vertex<scalar> *second) : vertices{first, second}, adjacent_faces() {}
    /**
     * @brief Get the other vertex as v
     * 
     * @param v vertex
     * @return the other vertex in this edge
     */
    const vertex<scalar> *get_not(const vertex<scalar> *v) const
    {
        if (v == vertices[0])
        {
            return vertices[1];
        }
        if (v == vertices[1])
        {
            return vertices[0];
        }
        assert(false && "v is neither");
    }
};

/**
 * @brief triangle struct
 * 
 * @tparam _scalar type of entry, mostly double, float
 */
template <typename _scalar>
struct triangle
{
    using scalar = _scalar;
    std::array<const vertex<scalar> *, 3> vertices; ///< which vertices are inside this triangle
    std::array<const edge<scalar> *, 3> edges;      ///< which edges are the boundary of this triangle
    /**
     * @brief Construct a new triangle object
     * 
     * @param first vertex 1
     * @param second vertex 2
     * @param third vertex 3
     */
    triangle(const vertex<scalar> *first, const vertex<scalar> *second, const vertex<scalar> *third) : vertices{first, second, third} {}
    /**
     * @brief whatever this does? maybe something with transformation
     * 
     * @return std::pair<Eigen::Matrix<scalar, 2, 2>, Eigen::Matrix<scalar, 2, 1>> 
     */
    std::pair<Eigen::Matrix<scalar, 2, 2>, Eigen::Matrix<scalar, 2, 1>> transformation() const
    {
        Eigen::Matrix<scalar, 2, 2> mat;
        Eigen::Matrix<scalar, 2, 1> vec;
        mat.col(0) = vertices[0]->pos - vertices[2]->pos;
        mat.col(1) = vertices[1]->pos - vertices[2]->pos;
        vec = vertices[2]->pos;
        return std::make_pair(mat, vec);
    }
    /**
     * @brief get area of this triangle
     * 
     * @return area
     */
    scalar area() const
    {
        matrix2<scalar> mat;
        mat.col(0) = vertices[1]->pos - vertices[0]->pos;
        mat.col(1) = vertices[2]->pos - vertices[0]->pos;
        using std::abs;
        return abs(mat.determinant());
    }
};
/**
 * @brief struct to hold a mesh
 * 
 * @tparam _scalar 
 */
template <typename _scalar>
struct mesh
{
    using scalar = _scalar;
    std::vector<vertex<scalar>> vertices; ///< all vertices
    std::vector<edge<scalar>> edges;      ///< all edges
    std::vector<triangle<scalar>> cells;  ///< all cells
    /**
     * @brief Construct a new mesh object
     * 
     */
    mesh() {}
    /**
     * @brief Construct a new mesh object
     * 
     */
    mesh(const mesh &) = delete;
    /**
     * @brief not needed here
     * 
     * @return mesh& 
     */
    mesh &operator=(const mesh &) = delete;
    /**
     * @brief Construct a new mesh object
     * 
     */
    mesh(mesh &&) = default;
    /**
     * @brief move assignement operator should be default
     * 
     * @return mesh& reference to other mesh
     */
    mesh &operator=(mesh &&) = default;
    /**
     * @brief get all boundary vertices
     * 
     * @return std::unordered_set<size_t> set of all vertices at boundary
     */
    std::unordered_set<size_t> boundary_vertices() const
    {
        std::unordered_set<size_t> set;
        for (size_t i = 0; i < vertices.size(); i++)
        {
            for (size_t ih = 0; ih < vertices[i].connected_edges.size(); ih++)
            {
                if (vertices[i].connected_edges[ih]->adjacent_faces.size() == 1)
                {
                    set.insert(i);
                    break;
                }
            }
        }
        return set;
    }
};

/**
 * @brief get the mesh from the mesh builder
 * 
 * @tparam scalar 
 * @return mesh<scalar> 
 */
template <typename scalar>
inline mesh<scalar> mesh_builder<scalar>::get_mesh()
{
    mesh<scalar> ret;
    size_t i = 0;
    // save all vertices inside the new mesh
    for (vector2<scalar> vert : vertices)
    {
        ret.vertices.emplace_back(vert, i++);
    }
    // reserve space for edges
    ret.edges.reserve(this->edges.size());
    std::unordered_map<pear<size_t>, edge<scalar> *> edge_map; // map of edges
    // save all edges inside the mesh
    for (auto it = edges.begin(); it != edges.end(); it++)
    {
        size_t index1 = std::get<0>(*it);
        size_t index2 = std::get<1>(*it);
        ret.edges.emplace_back(ret.vertices.data() + index1, ret.vertices.data() + index2);
        edge<scalar> *pushed = ret.edges.data() + (ret.edges.size() - 1);
        ret.vertices[index1].connected_edges.push_back(pushed);
        ret.vertices[index2].connected_edges.push_back(pushed);
        edge_map.insert(std::make_pair(std::make_tuple(index1, index2), pushed));
    }
    // reserve space for cells
    ret.cells.reserve(this->cells.size());
    // store all the cells inside the mesh
    for (auto it = cells.begin(); it != cells.end(); it++)
    {
        size_t index1 = std::get<0>(*it);
        size_t index2 = std::get<1>(*it);
        size_t index3 = std::get<2>(*it);
        ret.cells.emplace_back(ret.vertices.data() + index1, ret.vertices.data() + index2, ret.vertices.data() + index3);
        triangle<scalar> *pushed = ret.cells.data() + (ret.cells.size() - 1);
        edge_map[std::make_tuple(index1, index2)]->adjacent_faces.push_back(pushed);
        edge_map[std::make_tuple(index2, index3)]->adjacent_faces.push_back(pushed);
        edge_map[std::make_tuple(index1, index3)]->adjacent_faces.push_back(pushed);
        ret.vertices[index1].connected_cells.push_back(pushed);
        ret.vertices[index2].connected_cells.push_back(pushed);
        ret.vertices[index3].connected_cells.push_back(pushed);
    }
    return ret;
}
/**
 * @brief Rectangular mesh
 * 
 * @tparam scalar 
 * @param n how many vertices this mesh has in each direction
 * @param dim coordinates of the maximum coordinate (from 0,0 up to dim(0),dim(1))
 * @return mesh<scalar> 
 */
template <typename scalar>
mesh<scalar> semiquad_mesh(size_t n, const vector2<scalar> &dim)
{
    std::vector<std::vector<size_t>> vertex_table(n + 1, std::vector<size_t>(n + 1, -1));
    mesh_builder<scalar> builder;
    for (size_t i = 0; i <= n; i++)
    {
        for (size_t ih = 0; ih <= n; ih++)
        {
            vertex_table[i][ih] = builder.add_vertex((dim(1) * i) / n, (dim(0) * ih) / n);
        }
    }
    for (size_t i = 0; i < n; i++)
    {
        for (size_t ih = 0; ih < n; ih++)
        {
            builder.add_edge(vertex_table[i][ih], vertex_table[i + 1][ih + 1]);
            builder.add_cell(vertex_table[i][ih], vertex_table[i][ih + 1], vertex_table[i + 1][ih + 1]);
            builder.add_cell(vertex_table[i][ih], vertex_table[i + 1][ih], vertex_table[i + 1][ih + 1]);
        }
    }
    return builder.get_mesh();
}
/**
 * @brief get a rectangular mesh from two max coordinates
 * 
 * @tparam scalar 
 * @param n dimension of mesh
 * @param x max coord 1 value
 * @param y max coord 2 value
 * @return mesh<scalar> 
 */
template <typename scalar>
mesh<scalar> semiquad_mesh(size_t n, scalar x, scalar y)
{
    vector2<double> dim;
    dim(0) = x;
    dim(1) = y;
    return semiquad_mesh<scalar>(n, dim);
}

#endif
