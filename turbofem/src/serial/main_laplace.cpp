#include "laplace_equation.hpp"

int main()
{
    int gridres;
    std::cout << "Enter grid resolution: ";
    std::cin >> gridres;
    std::cout << std::endl;

    solve_laplace_equation(gridres);
}
