#ifndef TIMESTEP_HPP
#define TIMESTEP_HPP

#include "vector2.hpp"
#include <Eigen/Sparse>

/**
 * @brief Implements Runge-Kutta-4 (explicit)
 * 
 * @param u heat solution at indices
 * @param M mass matrix
 * @param A stiffness matrix
 * @param iter iterations
 * @param dt timestep
 */
void rk4(vectorX<double> &u, const Eigen::SparseMatrix<double> &M, const Eigen::SparseMatrix<double> &A, const unsigned &iter, const double &dt)
{
    Eigen::SparseLU<Eigen::SparseMatrix<double>> MLU(M);
    for (unsigned t = 0; t < iter; t++)
    {
        vectorX<double> k1 = MLU.solve(-A * u);
        vectorX<double> k2 = MLU.solve(-A * u + dt / 2.0 * k1);
        vectorX<double> k3 = MLU.solve(-A * u + dt / 2.0 * k2);
        vectorX<double> k4 = MLU.solve(-A * u + dt * k3);
        u = u + dt * 1. / 6. * (k1 + 2.0 * k2 + 2 * k3 + k4);
    }
}
/**
 * @brief Implements Adam-Bashforth multistep
 * 
 * @param u heat solution at indices
 * @param M mass matrix
 * @param A stiffness matrix
 * @param iter iterations
 * @param dt timestep, must be below 1e-6
 */
void multistep_adam_bashforth(vectorX<double> &u, const Eigen::SparseMatrix<double> &M, const Eigen::SparseMatrix<double> &A, const unsigned &iter, const double &dt)
{
    Eigen::SparseLU<Eigen::SparseMatrix<double>> MLU(M);

    vectorX<double> un_1(u.size()), un_2(u.size()), un_3(u.size()), un_4(u.size()), un_5(u.size());

    for (unsigned t = 0; t < iter; t++)
    {
        switch (t)
        {
        case 0:
        {
            un_1 = u + dt * MLU.solve(-A * u); // solve explicit euler with smaller timestep
            break;
        }
        case 1:
        {
            un_2 = un_1 + dt * (3. / 2. * MLU.solve(-A * un_1) - 0.5 * MLU.solve(-A * u));
            break;
        }
        case 2:
        {
            un_3 = un_2 + dt * (23. / 12. * MLU.solve(-A * un_2) - 16. / 12. * MLU.solve(un_1) + 5. / 12. * MLU.solve(u));
            break;
        }
        case 3:
        {
            un_4 = un_3 + dt * (55. / 24. * MLU.solve(-A * un_3) - 59. / 24. * MLU.solve(-A * un_2) + 37. / 24. * MLU.solve(-A * un_1) - 9. / 24. * MLU.solve(u));
            break;
        }
        default:
        {
            // do this step allways if previous are there
            un_5 = un_4 + dt * (1901. / 720. * MLU.solve(-A * un_4) - 2774. / 720. * MLU.solve(-A * un_3) + 2616. / 720. * MLU.solve(-A * un_2) - 1274. / 720. * MLU.solve(un_1) + 251. / 720. * MLU.solve(-A * u));
            u = un_1;
            un_1 = un_2;
            un_2 = un_3;
            un_3 = un_4;
            un_4 = un_5;
        }
        }
    }

    switch (iter)
    {
    case 0:
        break;
    case 1:
        u = un_1;
        break;
    case 2:
        u = un_2;
        break;
    case 3:
        u = un_3;
        break;
    case 4:
        u = un_4;
        break;
    default:
        u = un_5;
    }
}

/**
 * @brief Implements DIRK2 (diagonally implicit runge kutta midpoint)
 * 
 * @param u heat solution at indices
 * @param M mass matrix
 * @param A stiffness matrix
 * @param iter iterations
 * @param dt timestep
 */
void dirk2(vectorX<double> &u, const Eigen::SparseMatrix<double> &M, const Eigen::SparseMatrix<double> &A, const unsigned &iter, const double &dt)
{
    Eigen::SparseLU<Eigen::SparseMatrix<double>> implLU(M + 0.5 * dt * A);

    for (unsigned t = 0; t < iter; t++)
    {
        u = implLU.solve(M * u - 0.5 * dt * A * u);
    }
}

#endif