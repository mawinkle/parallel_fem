#include <fstream>

/**
 * @brief Prints the solution vector to a file in a format suitable for plotting.
 * @param gridres The resolution of the grid
 * @param mu The solution vector
 * @param filename The filename to save to
 */
void print_for_plot(const int gridres, const vectorX<double>& mu, const std::string filename)
{
    Eigen::MatrixXd plotout(gridres + 1, gridres + 1);
    for (size_t i = 0; i <= gridres; i++)
    {
        for (size_t j = 0; j <= gridres; j++)
        {
            plotout(i, j) = mu[i * (1+gridres) + j];
        }
    }

    std::ofstream plot_stream(filename);

    for (size_t i = 0; i < plotout.rows(); i++)
    {
        for (size_t j = 0; j < plotout.cols(); j++)
        {
            plot_stream << plotout(i, j) << " ";
        }
        plot_stream << "\n";
    }
    plot_stream.close();
}
