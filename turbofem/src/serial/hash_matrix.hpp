#ifndef HASH_MATRIX_HPP
#define HASH_MATRIX_HPP
#include <unordered_map>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <cassert>
/**
 * @brief struct to hold a sparse hash matrix
 * 
 * @tparam _scalar 
 */
template <typename _scalar>
struct hash_matrix
{
    using scalar = _scalar;
    size_t m, n;
    /**
     * @brief Construct a new hash matrix object
     * 
     * @param _m how many rows
     * @param _n how many cols
     */
    hash_matrix(size_t _m, size_t _n) : m(_m), n(_n)
    {
    }
    std::unordered_map<size_t, std::unordered_map<size_t, scalar>> entries;
    /**
     * @brief get a coefficient reference
     * 
     * @param i coord i
     * @param j coord j
     * @return scalar& 
     */
    scalar &coeffRef(size_t i, size_t j)
    {
        assert(i < m && j < n);
        if (entries.find(i) == entries.end())
        {
            entries.insert(std::make_pair(i, std::unordered_map<size_t, scalar>()));
        }
        if (entries[i].find(j) == entries[i].end())
        {
            entries[i].insert(std::make_pair(j, 0));
        }
        return entries[i][j];
    }
    /**
     * @brief does this entry exist in this hash map
     * 
     * @param i coord to check
     * @param j coord to check
     * @return true if the entry was found
     * @return false if it was not found
     */
    bool has(size_t i, size_t j)
    {
        assert(i < m && j < n);
        if (entries.find(i) == entries.end())
            return false;
        if (entries[i].find(j) == entries[i].end())
            return false;
        return true;
    }
    /**
     * @brief erase an entry from this matrix
     * 
     * @param i coord to erase
     * @param j coord to erase
     */
    void erase(size_t i, size_t j)
    {
        assert(i < m && j < n);
        if (has(i, j))
        {
            entries[i].erase(j);
        }
    }
    /**
     * @brief Set the i row to identity
     * 
     * @param i this row should be all 0 except at column i
     */
    void set_id_row(size_t i)
    {
        assert(i < m);
        if (entries.find(i) == entries.end())
            entries.insert(std::make_pair(i, std::unordered_map<size_t, scalar>()));
        else
            entries[i].clear();
        entries[i].insert(std::make_pair(i, 1));
    }
    /**
     * @brief get dense representation of this hash_matrix
     * 
     * @return Eigen::Matrix<scalar, Eigen::Dynamic, Eigen::Dynamic> 
     */
    Eigen::Matrix<scalar, Eigen::Dynamic, Eigen::Dynamic> toDense() const
    {
        Eigen::Matrix<scalar, Eigen::Dynamic, Eigen::Dynamic> ret(m, n);
        for (const std::pair<size_t, std::unordered_map<size_t, scalar>> &map : entries)
        {
            for (const std::pair<size_t, scalar> &entryp : map.second)
            {
                ret(map.first, entryp.first) = entryp.second;
            }
        }
        return ret;
    }
    /**
     * @brief get sparse representation of this hash_matrix
     * 
     * @return Eigen::SparseMatrix<scalar> 
     */
    Eigen::SparseMatrix<scalar> toSparse() const
    {
        Eigen::SparseMatrix<scalar> ret(m, n);
        for (const std::pair<size_t, std::unordered_map<size_t, scalar>> &map : entries)
        {
            for (const std::pair<size_t, scalar> &entryp : map.second)
            {
                ret.coeffRef(map.first, entryp.first) = entryp.second;
            }
        }
        return ret;
    }
};
#endif