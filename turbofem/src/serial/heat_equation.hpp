#include "mesh.hpp"
#include "assembler.hpp"
#include "providers.hpp"
#include "timestep.hpp"
#include "printing.hpp"
#include <cmath>
#include <fstream>

void initial_condition(const mesh<double>& me, vectorX<double>& mu)
{
    for (size_t i = 0; i < mu.rows(); i++)
    {
        double posx = me.vertices[i].pos(0);
        double posy = me.vertices[i].pos(1);
        double val = (posx > 0.4 && posx < 0.6) ? 1 : 0;
        if (posx < 0.5)
        {
            mu(i) = std::exp(-3. * ((posx - .25) * (posx - .25) + (posy - .5) * (posy - .5)));
        }
        else
        {
            mu(i) = std::exp(-3. * ((posx - .75) * (posx - .75) + (posy - .5) * (posy - .5)));
        }
        assert(mu(i) != 0);
    }
}

/**
 * @brief Solves the heat equation u' - lapl u = 0 with essential boundary conditions.
 * @param iter The number of timestepping iterations to use
 * @param gridres The resolution of the grid, i.e. the number of nodes per dimension minus 1
 */
vectorX<double> solve_heat_equation(size_t iter, int gridres)
{
    double dt = 1e-3;
    dt = std::min(0.5 / gridres / gridres, dt);
    std::cout << "using dt=" << dt << std::endl;

    mesh<double> me = semiquad_mesh<double>(gridres, 1, 1);

    matrix_assembler<double> asmb(me);
    Eigen::SparseMatrix<double> M = asmb.assemble<mass_matrix_provider<double>>().toSparse();
    Eigen::SparseMatrix<double> A = asmb.assemble<stiffness_matrix_provider<double>>().toSparse();

    vectorX<double> mu(me.vertices.size());
    initial_condition(me, mu);
    print_for_plot(gridres, mu, "u0.dat");

    // do timestepping
    //rk4(mu, M, A, iter, dt);
    dirk2(mu, M, A, iter, dt);

    print_for_plot(gridres, mu, "u.dat");

    std::cout << mu.sum() << std::endl;

    return mu;
}
