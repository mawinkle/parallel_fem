#ifndef VECTOR2_HPP
#define VECTOR2_HPP
#include <Eigen/Dense>
template <typename T>
using vector2 = Eigen::Matrix<T, 2, 1>;
template <typename T>
using vector3 = Eigen::Matrix<T, 3, 1>;
template <typename T>
using matrix3 = Eigen::Matrix<T, 3, 3>;
template <typename T>
using matrix2 = Eigen::Matrix<T, 2, 2>;
template <typename T>
using matrixX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;
template <typename T>
using vectorX = Eigen::Matrix<T, Eigen::Dynamic, 1>;
#endif