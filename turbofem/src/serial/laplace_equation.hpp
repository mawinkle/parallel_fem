#include "mesh.hpp"
#include "assembler.hpp"
#include "providers.hpp"
#include "timestep.hpp"
#include "printing.hpp"
#include <cmath>
#include <fstream>

struct f
{
  double operator()(vector2<double> v) const
  {
    const vector2<double> mid{0.5, 0.5};
    return std::exp((v - mid).norm() * -10);
  }
};

void apply_dirichlet_boundary_condition(const mesh<double>& x, hash_matrix<double>& galmat, Eigen::VectorXd& rhs)
{
    auto dx = x.boundary_vertices();
    for (auto idx : dx)
    {
        galmat.set_id_row(idx);
        rhs(idx) = 0; //std::sin(7.2*(x.vertices[idx].pos(0) + x.vertices[idx].pos(1)));
    }
  }

/**
 * @brief Solves the laplace equation lapl u = f with Dirichlet boundary conditions.
 * @param gridres The resolution of the grid, i.e. the number of nodes per dimension minus 1
 */
Eigen::VectorXd solve_laplace_equation(int gridres)
{
    using namespace Eigen;

    mesh<double> x = semiquad_mesh<double>(gridres, 1, 1);

    matrix_assembler<double> a(x);
    hash_matrix<double> galmat = a.assemble<stiffness_matrix_provider<double>>();

    vector_assembler<double> v(x);
    VectorXd rhs = v.assemble<load_provider<double, f>>();

    apply_dirichlet_boundary_condition(x, galmat, rhs);

    SparseMatrix<double> egalmat = galmat.toSparse();
    SparseLU<SparseMatrix<double>> splu(egalmat);
    VectorXd sol = splu.solve(rhs);

    print_for_plot(gridres, sol, "lapl.dat");

    return sol;
}
