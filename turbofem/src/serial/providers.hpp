#ifndef PROVIDERS_HPP
#define PROVIDERS_HPP

#include "vector2.hpp"

/**
 * @brief provider for grad u * grad v
 * 
 * @tparam scalar 
 */
template <typename scalar>
struct stiffness_matrix_provider
{
    Eigen::Matrix<scalar, 3, 3> eval(const triangle<scalar> &tria)
    {
        matrix3<scalar> gbhelper;
        gbhelper.col(0) = vector3<scalar>::Ones();
        gbhelper.template block<1, 2>(0, 1) = tria.vertices[0]->pos.transpose();
        gbhelper.template block<1, 2>(1, 1) = tria.vertices[1]->pos.transpose();
        gbhelper.template block<1, 2>(2, 1) = tria.vertices[2]->pos.transpose();
        matrix3<scalar> gbinv = gbhelper.inverse();
        matrix3<scalar> element_mat = gbinv.template block<2, 3>(1, 0).transpose() * gbinv.template block<2, 3>(1, 0);
        element_mat *= tria.area();
        return element_mat;
    }
};
/**
 * @brief provider for u' * v
 * 
 * @tparam scalar 
 */
template <typename scalar>
struct mass_matrix_provider
{
    Eigen::Matrix<scalar, 3, 3> eval(const triangle<scalar> &tria)
    {
        matrix3<scalar> mass_mat;
        mass_mat << 2, 1, 1,
            1, 2, 1,
            1, 1, 2;
        mass_mat *= tria.area();
        mass_mat /= scalar(12);
        return mass_mat;
    }
};
/**
 * @brief provider for f * v
 * 
 * @tparam scalar 
 * @tparam functor 
 */
template <typename scalar, typename functor>
struct load_provider
{
    vector3<scalar> eval(const triangle<scalar> &o)
    {
        vector3<scalar> a;
        functor f;
        scalar area = o.area();
        for (size_t i = 0; i < o.vertices.size(); i++)
        {
            a(i) = f(o.vertices[i]->pos);
        }
        a *= area;
        a /= scalar(3);
        return a;
    }
};

#endif