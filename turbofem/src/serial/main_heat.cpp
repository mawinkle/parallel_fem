#include "heat_equation.hpp"

int main()
{
    size_t iter;
    std::cout << "Enter iterations: ";
    std::cin >> iter;
    std::cout << std::endl;

    int gridres;
    std::cout << "Enter grid resolution: ";
    std::cin >> gridres;
    std::cout << std::endl;

    solve_heat_equation(iter, gridres);
}
