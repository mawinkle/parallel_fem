#ifndef ASSEMBLER_HPP
#define ASSEMBLER_HPP
#include "mesh.hpp"
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <type_traits>
#include "hash_matrix.hpp"

/**
 * @brief Assemble a matrix from a matrix_provider
 * 
 * @tparam scalar 
 */
template<typename scalar>
struct matrix_assembler{
    const mesh<scalar>& m_mesh; ///< the mesh
    std::vector<Eigen::Matrix<double, 3, 3>> element_matrices;
    /**
     * @brief Construct a new matrix assembler object
     * 
     * @param m the mesh on where to assemble
     */
    matrix_assembler(const mesh<scalar>& m) : m_mesh(m), element_matrices(m.cells.size()){}
    /**
     * @brief assemble the matrix with given element_matrix_provider_t
     * 
     * @tparam element_matrix_provider_t functor with an eval function inside
     * @return hash_matrix<scalar> 
     */
    template<typename element_matrix_provider_t>
    hash_matrix<scalar> assemble(){
        element_matrix_provider_t provider;
        size_t i = 0;
        for(const triangle<scalar>& trian : m_mesh.cells){
            element_matrices[i++] = provider.eval(trian);
        }
        hash_matrix<scalar> mat(m_mesh.vertices.size(), m_mesh.vertices.size());
        for(i = 0;i < m_mesh.cells.size();i++){
            Eigen::Matrix<scalar, 3, 3>& elmat = element_matrices[i];
            size_t glob_indices[3] = {m_mesh.cells[i].vertices[0]->index, m_mesh.cells[i].vertices[1]->index, m_mesh.cells[i].vertices[2]->index};
            for(size_t x = 0;x < 3;x++){
                for(size_t y = 0;y < 3;y++){
                    mat.coeffRef(glob_indices[x], glob_indices[y]) += elmat(x, y);
                }
            }
        }
        return mat;
    }
};
/**
 * @brief assemble vector load
 * 
 * @tparam scalar 
 */
template<typename scalar>
struct vector_assembler{
    const mesh<scalar>& m_mesh;
    /**
     * @brief Construct a new vector assembler object
     * 
     * @param m the mesh
     */
    vector_assembler(const mesh<scalar>& m) : m_mesh(m){}
    /**
     * @brief assemble vector load with given element_vector_provider_t
     * 
     * @tparam element_vector_provider_t the type for the vector load, needs an eval function
     * @return vectorX<scalar> 
     */
    template<typename element_vector_provider_t>
    vectorX<scalar> assemble(){
        element_vector_provider_t provider;
        vectorX<scalar> ret = vectorX<scalar>::Zero(m_mesh.vertices.size());
        for(const triangle<scalar>& trian : m_mesh.cells){
            vector3<scalar> el = provider.eval(trian);
            size_t glob_indices[3] = {trian.vertices[0]->index, trian.vertices[1]->index, trian.vertices[2]->index};
            ret(glob_indices[0]) += el(0);
            ret(glob_indices[1]) += el(1);
            ret(glob_indices[2]) += el(2);
        }
        return ret;
    }
};

#endif