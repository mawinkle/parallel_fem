#ifndef MPIOXX_HPP
#define MPIOXX_HPP
#include <iostream>
#include <boost/circular_buffer.hpp>
#include <algorithm>
#include <vector>
#include <mpi.h>
namespace mpioxx{
template<typename T>
struct expanding_circular_buffer : public boost::circular_buffer<T>{
    using super = boost::circular_buffer<T>;
    using size_type = typename super::size_type;
    expanding_circular_buffer(size_t s) : super(s){}
    void push_back(T&& arg){
        if(super::capacity() == super::size()){
            expanding_circular_buffer copy(capacity() * 2);
            for(size_t i = 0;i < size();i++){
                copy.push_back(this->operator[](i));
            }
            swap(*this, copy);
        }
        super::push_back(std::move(arg));
    }
    void push_back(const T& arg){
        if(super::capacity() == super::size()){
            expanding_circular_buffer copy(capacity() * 2);
            for(size_t i = 0;i < size();i++){
                copy.push_back(this->operator[](i));
            }
            swap(*this, copy);
        }
        super::push_back(arg);
    }
    size_type size()const{
        return super::size();
    }
    size_type capacity()const{
        return super::capacity();
    }
    void clear(){
        super::clear();
    }
    auto begin(){
        return super::begin();
    }
    auto end(){
        return super::end();
    }
    auto empty(){
        return super::empty();
    }
};
struct mpi_streambuf : std::streambuf{
    size_t send_limit;
	int receiver;
    expanding_circular_buffer<char> buf;
    virtual std::streamsize xsputn(const char_type* s, std::streamsize count){
        for(size_t i = 0;i < count;i++){
            buf.push_back(*(s++));
        }
        if(buf.size() >= send_limit){
            sync();
        }
        return count;
    }
    virtual int sync()override{
        std::vector<char> text(buf.size());
        std::copy(buf.begin(), buf.end(), text.begin());
        MPI_Send(text.data(), text.size(), MPI_CHAR, 0, 0, MPI_COMM_WORLD);
        buf.clear();
        return 0;
    }
    
    mpi_streambuf() : std::streambuf(), send_limit(2048),receiver(0), buf(send_limit){}
    virtual int overflow(int c)override{
        buf.push_back((char)c);
        if(buf.size() >= send_limit){
            sync();
        }
        return 0;
    }
    virtual int underflow()override{
        if(buf.size() == 0){
            char a[1024];
            MPI_Status status;
            MPI_Recv((void*)a, 1024, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
            int n;
            MPI_Get_count(&status, MPI_CHAR, &n);
            for(int i = 0;i < n;i++){
                buf.push_back(a[i]);
            }
        }
        char ret = buf.front();
        return ret;
    }
    virtual int uflow()override{
        if(buf.size() == 0){
            char a[1024];
            MPI_Recv((void*)a, 1024, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for(size_t i = 0;i < 1024;i++){
                buf.push_back(a[i]);
            }
        }
        char ret = buf.front();
        buf.pop_front();
        return ret;
    }
    virtual std::streamsize xsgetn(char_type* s, std::streamsize count)override{
        for(size_t i = 0;i < count;i++){
            *(s++) = buf.front();
            buf.pop_front();
        }
        return count;
    }
    virtual std::streamsize showmanyc()override{
        return buf.size();
    }
    virtual int_type pbackfail(int_type c = EOF)override{
        return 0;
    }
};
}
#endif