#ifndef MPIOSTREAM_HPP
#define MPIOSTREAM_HPP
#include "mpioxx.hpp"
#include <iostream>
namespace mpioxx{
struct send_changer{
	int rec;
	constexpr send_changer(int _rec) : rec(_rec){}
};
constexpr send_changer broadcast(-1);
struct mpiostream : public std::ostream{
	mpi_streambuf* m_buf;
	mpiostream(mpi_streambuf* buf) : std::ostream(buf), m_buf(buf){
		
	}
	/*mpiostream& operator<<(const send_changer& sc){
		flush();
		m_buf.change_dest(sc.rec);
	}*/
};
using mpiistream = std::istream;
}
#endif