#include <turbofem/src/serial/mesh.hpp>
#include <mpi.h>
#include <iostream>

#include <turbofem/src/serial/assembler.hpp>
#include <turbofem/src/serial/providers.hpp>

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank;
    int size;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int k = std::sqrt(size);

    if (k * k != size)
    {
        std::cout << "Not square" << std::endl;
        exit(1);
    }

    double frac = 1.0 / k;
    mesh<double> mesh = semiquad_mesh<double>(100, 1.0, 1.0);

    matrix_assembler<double> asmb(mesh);
    Eigen::SparseMatrix<double> M = asmb.assemble<mass_matrix_provider<double>>().toSparse();
    Eigen::SparseMatrix<double> A = asmb.assemble<stiffness_matrix_provider<double>>().toSparse();

    // eigene boundaries anschauen
    // boundary contributions an höhere rank's senden
    std::unordered_set<size_t> b_vertices = mesh.boundary_vertices();

    for (auto i : b_vertices)
    {
        for (auto &e : mesh.vertices[i].connected_edges)
        {
            vertex<double> other = *e->get_not(&mesh.vertices[i]);
            if (b_vertices.contains(other.index))
            {
                // we have found an edge on the boundary
                if (e->tag & TOP)
                {
                    // our edge is on top
                    if (e->tag & LEFT)
                    {
                        // topleft
                    }
                    else if (e->tag & RIGHT)
                    {
                        // topright
                    }
                    else
                    {
                        // top
                    }
                }
                else if (e->tag & BOTTOM)
                {
                    // our edge is on bottom
                    if (e->tag & LEFT)
                    {
                        // bottomleft
                    }
                    else if (e->tag & RIGHT)
                    {
                        // bottomright
                    }
                    else
                    {
                        // bottom
                    }
                }
                else if (e->tag & LEFT)
                {
                    // only left
                }
                else if (e->tag & RIGHT)
                {
                    // only right
                }
                else
                {
                    // not on boundary...
                    // should never happen
                }

                break;
            }
        }
    }

    /*
    int my_vertices[mesh.vertices.size()];
    for (int i = 0; i < mesh.vertices.size(); i++)
    {
        my_vertices[i] = mesh.vertices[i].index;
    }

    int *vertices;
    if (rank == 0)
    {
        vertices = new int[size * mesh.vertices.size()];
    }
    MPI_Gather(my_vertices, mesh.vertices.size(), MPI_INT, vertices, size * mesh.vertices.size(), MPI_INT, 0, MPI_COMM_WORLD);
    */

    MPI_Finalize();
    return 0;
}
