#include <turbofem/src/serial/mesh.hpp>
#include <turbofem/src/mpi/mpi_mesh.hpp>
#include <iostream>
#include <cmath>
#include <mpi.h>
#include <cstddef>

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank;
    int size;

    // get mesh size
    int vs, es, cs; // vertices size, edges size, cells size
    readSizeFromFile("../turbofem/src/mesh.msh", &vs, &es, &cs);
    // create mesh type
    MPI_Datatype mesh_type;
    MPI_Datatype type[13] = {MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_DOUBLE, MPI_DOUBLE, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT};
    int blocklen[13] = {1, 1, 1, vs, vs, vs, es, es, es, cs, cs, cs, cs};
    MPI_Aint disp[13];
    disp[0] = offsetof(mpi_mesh, vs);
    disp[1] = offsetof(mpi_mesh, es);
    disp[2] = offsetof(mpi_mesh, cs);
    disp[3] = offsetof(mpi_mesh, v_index);
    disp[4] = offsetof(mpi_mesh, vx);
    disp[5] = offsetof(mpi_mesh, vy);
    disp[6] = offsetof(mpi_mesh, e_index);
    disp[7] = offsetof(mpi_mesh, e1);
    disp[8] = offsetof(mpi_mesh, e2);
    disp[9] = offsetof(mpi_mesh, c_index);
    disp[10] = offsetof(mpi_mesh, c1);
    disp[11] = offsetof(mpi_mesh, c2);
    disp[12] = offsetof(mpi_mesh, c3);
    MPI_Type_create_struct(13, blocklen, disp, type, &mesh_type);
    MPI_Type_commit(&mesh_type);
    // end create mesh type

    mpi_mesh mesh_;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // cells to compute for each processor
    int per_processor = cs / size;

    int send_cell_counts[size];
    int displacement_send_cells[size];

    // fill per processor amount to be equal
    std::fill(send_cell_counts, send_cell_counts + size, per_processor);

    for (int i = 0; i < size; i++)
    {
        displacement_send_cells[i] = per_processor * i;
    }

    send_cell_counts[size - 1] = cs - (size - 1) * per_processor; // set last one to all work that should be done by it

    // how many cells each rank should have
    int recv_cells[send_cell_counts[rank]];

    // read in mesh and send it to others
    if (rank == 0)
    {
        readMeshFromFile("../turbofem/src/mesh.msh", &mesh_);
    }

    // wait until rank 0 has read mesh
    MPI_Barrier(MPI_COMM_WORLD);

    // send mesh, all other will receive
    //MPI_Bcast(&mesh_, 1, mesh_type, 0, MPI_COMM_WORLD);
    MPI_Scatterv(mesh_.c_index, send_cell_counts, displacement_send_cells, MPI_INT, recv_cells, send_cell_counts[rank], MPI_INT, 0, MPI_COMM_WORLD);

    // wait until all above have been completed

    // iterate over recv_cells
    for (int i = 0; i < send_cell_counts[rank]; i++)
    {
        std::cout << "rank " << rank << " has cells after: " << recv_cells[i] << std::endl;
    }

    MPI_Finalize();
    return 0;
}
